<?php

namespace App\Console\Commands;

use App\Jobs\PurgeOldArchives;
use App\Jobs\Sheduled\QueueJob;
use App\Models\Project;
use Illuminate\Console\Command;

use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Foundation\Bus\DispatchesJobs;

class CleanOrphanMirrorsArchivesCommand extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deployer:orphans-purge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove Mirror folders and archives of old projects no more present';

    /**
     * @var Filesystem
     */
    private $filesystem;


    /**
     * Create a new command instance.
     *
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        parent::__construct();
        $this->filesystem = $filesystem;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $projects = Project::NotDeploying()->get();

        $this->removeDirectories($this->getArchives($projects));
        $this->removeDirectories($this->getMirrors($projects));

        $projects->each(function($project) {
            $this->dispatch(new QueueJob(new PurgeOldArchives(null, null, null, $project)));
        });
    }

    private function getArchives($projects)
    {
        $archives = $projects->map->relativeArchivePath();
        return collect($this->filesystem->directories('archives'))->diff($archives);
    }

    private function getMirrors($projects)
    {
        $mirrors = $projects->map->relativeMirrorPath();
        return collect($this->filesystem->directories('mirrors'))->diff($mirrors);
    }

    private function removeDirectories($dirs)
    {
        collect($dirs)->each(function($dir) {
            if ($this->filesystem->exists($dir)) {
                $this->filesystem->deleteDirectory($dir);
            }
        });
    }
}
