<?php

namespace App\Console\Commands;

use App\Models\DeployStep;
use App\Models\Project;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DefaultDeployStepsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deployer:default-steps';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset the Deploy steps to the default ones';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reset = $this->resetDB();
        if ($reset instanceof \Exception) {
            throw $reset($reset->getMessage());
        }

        $clone = DeployStep::create([
            'name'        => 'Clone',
            'description' => 'Mirror the repository and update the project branch commit refs',
            'step'        => DeployStep::CLONE
        ]);
        $clone->commands()->createMany([
            [
                'name'   => 'Mirror',
                'script' => 'BuildGitRepo',
                'custom' => false,
                'weight' => 10
            ],
            [
                'name'   => 'Update',
                'script' => 'UpdateRepoInfo',
                'custom' => false,
                'weight' => 20
            ]
        ]);
        $release = DeployStep::create([
            'name'        => 'ReleaseArchive',
            'description' => 'From the specified commit, clone the code and archive it',
            'step'        => DeployStep::ARCHIVE
        ]);
        $release->commands()->createMany([
            [
                'name'   => 'Archive',
                'script' => 'CreateReleaseArchive',
                'custom' => false,
                'weight' => 50
            ]
        ]);
        $send = DeployStep::create([
            'name'        => 'SendFIleToServers',
            'description' => 'Upload the release to the destination servers',
            'step'        => DeployStep::SEND
        ]);
        $send->commands()->createMany([
            [
                'name'   => 'Send',
                'script' => 'SendFileToServer',
                'custom' => false,
                'weight' => 20
            ],
            [
                'name'   => 'Extract',
                'script' => 'ExtractRelease',
                'custom' => false,
                'weight' => 30
            ]
        ]);
        $install = DeployStep::create([
            'name'        => 'InstallDependencies',
            'description' => 'Install composer/other types of dependencies',
            'step'        => DeployStep::INSTALL
        ]);
        $install->commands()->createMany([
            [
                'name'   => 'Composer',
                'script' => 'InstallComposerDependencies',
                'custom' => false,
                'weight' => 20
            ],
        ]);
        $activate = DeployStep::create([
            'name'        => 'ActivateRelease',
            'description' => 'Activate the newly created release',
            'step'        => DeployStep::ACTIVATE
        ]);
        $activate->commands()->createMany([
            [
                'name'   => 'Copy',
                'script' => 'CopyFiles',
                'custom' => false,
                'weight' => 30
            ],
            [
                'name'   => 'Share',
                'script' => 'LinkSharedFolders',
                'custom' => false,
                'weight' => 50
            ],
            [
                'name'   => 'Activate',
                'script' => 'ActivateRelease',
                'custom' => false,
                'weight' => 80
            ]
        ]);
        $clean = DeployStep::create([
            'name'        => 'CleanProject',
            'description' => 'Remove old releases',
            'step'        => DeployStep::CLEAN
        ]);
        $clean->commands()->createMany([
            [
                'name'   => 'PurgeReleases',
                'script' => 'PurgeOldReleases',
                'custom' => false,
                'weight' => 50
            ],
            [
                'name'   => 'PurgeArchives',
                'script' => 'PurgeOldArchives',
                'custom' => false,
                'weight' => 60
            ]
        ]);

        $this->attachToAllProjects([$clone, $release, $send, $install, $activate, $clean]);
    }

    private function resetDB()
    {
        return DB::transaction(function() {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
//            DB::table('deploy_step_project')->truncate();
            DB::table('commands')->truncate();
            DB::table('deploy_steps')->truncate();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        });
    }

    private function attachToAllProjects($deploySteps)
    {
        $deploySteps = collect($deploySteps)->pluck('id')->toArray();
        Project::all()->each(function($project) use($deploySteps) {
            $project->deploySteps()->attach($deploySteps);
        });
    }
}
