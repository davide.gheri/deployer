<?php

namespace App\Console\Commands;

use App\Console\Commands\Install\EnvWriter;
use App\Services\Scripts\Parser;
use App\Services\Scripts\Process;
use Illuminate\Config\Repository;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class GenerateSSHKeyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deployer:key';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate SSH Key';

    protected $filesystem;
    protected $writer;

    /**
     * Create a new command instance.
     * @param Filesystem $filesystem
     * @param EnvWriter $writer
     */
    public function __construct(Filesystem $filesystem, EnvWriter $writer)
    {
        parent::__construct();

        $this->filesystem = $filesystem;
        $this->writer = $writer;
    }

    /**
     * Execute the console command.
     * Generate the SSH key for the application if not already generated
     * Create the Wrapper file to use for every script that needs SSH connection
     *
     * @param Process $process
     * @param Parser $parser
     * @param Repository $config
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle(Process $process, Parser $parser, Repository $config)
    {
        $key_file = $config->get('deployer.ssh.id_rsa');
        $wrapper_file = $config->get('deployer.ssh.wrapper');

        $confirm = true;
        if ($this->filesystem->exists($key_file)) {
            $confirm = $this->confirm('id_rsa file already exists, overwrite?');
        }
        if ($confirm) {
            $process->setScript('GenerateSSHKey', [
                'key_file' => $key_file
            ])->run();
            if (!$process->isSuccessful()) {
                throw new \RuntimeException($process->getErrorOutput());
            }
            $this->getPublicFingerPrint($key_file);
        }

        $wrapper = $parser->parseFile('GitSSHWrapper', [
            'private_key' => $key_file,
        ], true);
        $this->filesystem->put($wrapper_file, $wrapper);
        $this->info('SSH wrapper file updated');
    }

    /**
     * Save the public fingerprint in the .env file,
     * print it in the console
     *
     * @param $key_file
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function getPublicFingerPrint($key_file)
    {
        $fingerPrint = $this->filesystem->get($key_file . '.pub');

        if(!$this->writer->save(['SSH' => ['FINGERPRINT' => '"' . trim($fingerPrint) . '"']])) {
            throw new \RuntimeException('Cannot save SSH key to env file');
        }
        $this->info('Fingerprint');
        $this->comment($fingerPrint);
        $this->info('Please include in ssh keys of git repo');
    }
}
