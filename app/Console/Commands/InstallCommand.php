<?php

namespace App\Console\Commands;

use App\Console\Commands\Install\EnvWriter;
use App\Models\User;
use DateTimeZone;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Helper\FormatterHelper;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deployer:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install the app';

    protected $filesystem;

    /**
     * Create a new command instance.
     *
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        parent::__construct();

        $this->filesystem = $filesystem;
    }

    /**
     * Execute the console command.
     *
     * @param EnvWriter $writer
     * @return mixed
     */
    public function handle(EnvWriter $writer)
    {
        if ($this->alreadyInstalled($writer)) {
            $this->info('Env file already set, skipping');
        } else {
            $writer->copy();
            $config = [
                'DB' => $this->setDatabaseConnection(),
                'APP' => $this->setAppInfo(),
                'MAIL' => $this->setMailSender(),
            ];
            if ($writer->save($config)) {
                $this->call('key:generate');
            } else {
                $this->error('Cannot write env file');
                return;
            }
        }
        $this->header('Migrating the db');
        $this->call('migrate:fresh');
        $this->callSilent('passport:install');
        $this->callSilent('passport:keys');

        $this->call('deployer:default-steps');

        $this->createAdminUser();
    }

    protected function createAdminUser()
    {
        $this->header('Create the Admin user');
        $data = [
            'name' => $this->ask('Name', 'Admin'),
            'email' => $this->ask('Email', 'admin@example.com'),
        ];
        $password = $this->secret('Password');
        while (empty($password)) {
            $this->warn('Password required');
            $password = $this->secret('Password');
        }
        $data['password'] = bcrypt($password);
        User::create($data);
        $this->info(sprintf('Admin User %s created', $data['name']));
    }

    protected function setDatabaseConnection()
    {
        $this->header('Setting up Database:');
        return [
            'HOST' => $this->ask('Host', 'localhost'),
            'PORT' => $this->ask('Port', 3306),
            'DATABASE' => $this->ask('Database name', 'deployer'),
            'USERNAME' => $this->ask('Username', 'deployer'),
            'PASSWORD' => $this->secret('Password'),
        ];
    }

    protected function setAppInfo()
    {
        $this->header('Setting up Application info');
        $info = [
            'NAME' => $this->ask('Name', 'Deployer'),
            'ENV' => $this->anticipate('Environment', ['local', 'production'], 'local'),
            'DEBUG' => $this->confirm('Debug enabled?', true) ? 'true' : 'false',
            'URL' => $this->ask('App url ("http://example.com")'),
        ];
        $regions = $this->getTimezoneRegions();
        $region = $this->choice('Timezone', array_keys($regions), 0);
        if ($region !== 'UTC') {
            $locations = $this->getTimeZoneLocations($regions[$region]);
            $region .= '/' . $this->choice('Timezone location', $locations, 0);
        }
        $info['TIMEZONE'] = $region;
        return $info;
    }

    protected function setMailSender()
    {
        $this->header('Setting up Mail info');
        return [
            'DRIVER' => $this->ask('Driver', 'smtp'),
            'HOST' => $this->ask('Host', 'smtp.mailtrap.io'),
            'PORT' => $this->ask('Port', '2525'),
            'USERNAME' => $this->ask('Username'),
            'PASSWORD' => $this->secret('Password'),
            'ENCRYPTION' => $this->ask('Encryption')
        ];
    }

    public function header($string)
    {
        $this->block($string, 'question');
    }

    public function block($message, $type = 'error')
    {
        $output = [];
        if (!is_array($message)) {
            $message = (array) $message;
        }
        foreach ($message as $msg) {
            $output[] = trim($msg);
        }
        $formatter = new FormatterHelper();
        $this->line($formatter->formatBlock($output, $type, true));
    }

    private function getTimezoneRegions()
    {
        return [
            'UTC'        => DateTimeZone::UTC,
            'Africa'     => DateTimeZone::AFRICA,
            'America'    => DateTimeZone::AMERICA,
            'Antarctica' => DateTimeZone::ANTARCTICA,
            'Asia'       => DateTimeZone::ASIA,
            'Atlantic'   => DateTimeZone::ATLANTIC,
            'Australia'  => DateTimeZone::AUSTRALIA,
            'Europe'     => DateTimeZone::EUROPE,
            'Indian'     => DateTimeZone::INDIAN,
            'Pacific'    => DateTimeZone::PACIFIC,
        ];
    }

    private function getTimeZoneLocations($region)
    {
        $locations = [];
        foreach (DateTimeZone::listIdentifiers($region) as $timezone) {
            $locations[] = substr($timezone, strpos($timezone, '/') + 1);
        }
        return $locations;
    }

    private function alreadyInstalled(EnvWriter $writer)
    {
        return $writer->checkEnvKey();
    }
}
