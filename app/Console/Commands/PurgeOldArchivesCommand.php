<?php

namespace App\Console\Commands;

use App\Jobs\PurgeOldArchives;
use App\Jobs\Sheduled\QueueJob;
use App\Models\DeployStep;
use App\Models\Project;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class PurgeOldArchivesCommand extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deployer:archives-purge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove old archives from storage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $projects = Project::notDeploying()->get();
        $projects->each(function($project) {
            $this->dispatch(new QueueJob(new PurgeOldArchives(null, null, null, $project)));
        });
    }
}
