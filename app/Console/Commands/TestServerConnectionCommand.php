<?php

namespace App\Console\Commands;

use App\Models\Server;
use App\Services\Scripts\Process;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TestServerConnectionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deployer:server-test {server?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'test an ssh connection to the specified Server, print the content of home directory';

    protected $process;

    /**
     * Create a new command instance.
     *
     * @param Process $process
     */
    public function __construct(Process $process)
    {
        parent::__construct();

        $this->process = $process;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        $server = $this->getServer();
        $this->process->setServer($server);
        $this->process->setScript('ls -a', [], false)->run();

        $this->line($this->process->getOutput());
    }

    protected function getServer()
    {
        if ($id = $this->argument('server')) {
            return Server::findOrFail($id);
        }
        $servers = Server::all()->keyBy('id');
        if ($servers->isEmpty()) {
            throw new ModelNotFoundException('No servers found!');
        }
        $id = $this->choice('Choose server', $servers->pluck('name', 'id')->toArray());

        return $servers[$id];
    }
}
