<?php

namespace App\Console\Commands;

use App\Jobs\BuildGitRepo;
use App\Jobs\Sheduled\QueueJob;
use App\Models\Project;
use Illuminate\Console\Command;

class UpdateGitRepoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deployer:update-git';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all git Mirrors';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $projects = Project::notDeploying()->get();
        $projects->each(function($project) {
            $this->dispatch(new QueueJob(new BuildGitRepo(null, null, $project)));
        });
    }
}
