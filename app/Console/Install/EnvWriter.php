<?php

namespace App\Console\Commands\Install;

use Illuminate\Filesystem\Filesystem;

class EnvWriter
{
    private $filesystem;
    private $env_file;

    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
        $this->env_file = base_path('.env');
    }

    public function copy()
    {
        if (!$this->checkEnvExists()) {
            $this->filesystem->copy(base_path('.env.example'), $this->env_file);
        }
    }

    public function save($config)
    {
        if ($this->checkEnvExists()) {
            $env = $this->filesystem->get($this->env_file);
            foreach ($config as $prefix => $data) {
                foreach ($data as $section => $value) {
                    $key = strtoupper($prefix . '_' . $section);
                    $env = preg_replace('/' . $key . '=(.*)/', $key . '=' . $value, $env);
                }
            }
            return $this->filesystem->put($this->env_file, trim($env) . PHP_EOL);
        } else {
            $this->copy();
            $this->save($config);
        }
    }

    public function checkEnvKey()
    {
        if ($this->checkEnvExists()) {
            return !empty(env('APP_KEY'));
        }
        return false;
    }

    private function checkEnvExists()
    {
        return $this->filesystem->exists($this->env_file);
    }
}