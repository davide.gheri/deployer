<?php

namespace App\Console;

use App\Console\Commands\CleanOrphanMirrorsArchivesCommand;
use App\Console\Commands\DefaultDeployStepsCommand;
use App\Console\Commands\GenerateSSHKeyCommand;
use App\Console\Commands\InstallCommand;
use App\Console\Commands\PurgeOldArchivesCommand;
use App\Console\Commands\TestServerConnectionCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        InstallCommand::class,
        GenerateSSHKeyCommand::class,
        DefaultDeployStepsCommand::class,
        CleanOrphanMirrorsArchivesCommand::class,
        PurgeOldArchivesCommand::class,
        TestServerConnectionCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('deployer:orphans-purge')
            ->daily();
        $schedule->command('deployer:archives-purge')
            ->daily();
        $schedule->command('deployer:update-git')
            ->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
