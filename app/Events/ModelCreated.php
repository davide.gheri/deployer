<?php

namespace App\Events;

class ModelCreated extends ModelEvent
{
    public $event_type = 'created';
}
