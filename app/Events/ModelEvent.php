<?php

namespace App\Events;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

abstract class ModelEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $model;
    public $name;
    private $channel;
    public $event_type;

    /**
     * Create a new event instance.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->name = strtolower(class_basename(get_class($model)));
        $this->channel = $this->getChannel();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel($this->getChannel());
    }

    /**
     * The event's broadcast name.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return $this->setChannelName();
    }

    public function setChannelName()
    {
        if(!empty($this->event_type)) {
            return $this->getChannel() . '.' . $this->event_type;
        }
        preg_match('/^Model([A-Za-z]+)$/', class_basename($this), $matches);

        return $this->getChannel() . '.' . (isset($matches[1]) ? strtolower($matches[1]) : 'default');
    }

    protected function getChannel()
    {
        $channel = defined(get_class($this->model) . '::CHANNEL') ?
            $this->model::CHANNEL : $this->name;
        return $channel;
    }
}