<?php

namespace App\Events;

class ModelUpdated extends ModelEvent
{
    public $event_type = 'updated';
}
