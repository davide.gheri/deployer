<?php

namespace App\Events\Observers;

use App\Events\ModelCreated;
use App\Events\ModelUpdated;
use Illuminate\Database\Eloquent\Model;

class ModelObserver
{
    public function created(Model $model)
    {
        broadcast(new ModelCreated($model))->toOthers();
    }

    public function updated(Model $model)
    {
        broadcast(new ModelUpdated($model));
    }

}