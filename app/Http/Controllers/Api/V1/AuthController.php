<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    /** @var Response $oauthResponse */
    protected $oauthResponse;

    protected function guard()
    {
        return Auth::guard('api');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @return JsonResponse
     */
    protected function sendLoginResponse(Request $request): JsonResponse
    {
        $this->clearLoginAttempts($request);

        return response()->json(json_decode($this->oauthResponse->getContent()));
    }

    /**
     * Attempt to log the user into the application.
     * Make an internal request to the Passport oauth/token endpoint
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request): bool
    {
        $email = $request->input('email');
        $password = $request->input('password');
        $client = $this->getClientCredentials();

        $newRequest = Request::create('/oauth/token', 'post', [
            'username' => $email,
            'password' => $password,
            'grant_type' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'scope' => '*'
        ]);

        try {
            $this->oauthResponse = app()->handle($newRequest);
            if ($this->oauthResponse->getStatusCode() === 200) {
                return true;
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    protected function getClientCredentials($type = 'password_client')
    {
        return DB::table('oauth_clients')->select('id', 'secret')->where('revoked', 0)->where($type, 1)->first();
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json(['message' => 'success']);
    }

    public function refresh(Request $request)
    {
        $this->validate($request, [
            'token' => 'required|string'
        ]);
        $token = $request->input('token');
        $client = $this->getClientCredentials();

        $newRequest = Request::create('/oauth/token', 'post', [
            'refresh_token' => $token,
            'grant_type' => 'refresh_token',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'scope' => '*'
        ]);

        try {
            $this->oauthResponse = app()->handle($newRequest);
            return response()->json(json_decode($this->oauthResponse->getContent()), $this->oauthResponse->getStatusCode());
        } catch (\Exception $e) {
            return response()->json($e, 401);
        }
    }

}
