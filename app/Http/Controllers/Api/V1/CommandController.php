<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Api\Command\CreateCommandRequest;
use App\Http\Requests\Api\Command\UpdateCommandRequest;
use App\Models\Command;
use App\Models\DeployStep;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $project_id
     * @param $deployStep_id
     * @return \Illuminate\Http\Response
     */
    public function index($project_id, $deployStep_id)
    {
        return DeployStep::findOrFail($deployStep_id)->commands()
            ->where(function($q) use($project_id) {
              $q->whereNull('project_id')
                  ->orWhere('project_id', $project_id);
            })
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateCommandRequest $request
     * @param $project_id
     * @param $deployStep_id
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCommandRequest $request, $project_id, $deployStep_id)
    {
        $deployStep = DeployStep::findOrFail($deployStep_id);
        $data = $request->only(['name', 'script', 'custom', 'weight']);
        $data['custom'] = 1; //Force custom
        $data['project_id'] = $project_id;
        return $deployStep->commands()->create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCommandRequest $request
     * @param $project_id
     * @param $deployStep_id
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCommandRequest $request, $project_id, $deployStep_id, $id)
    {
        $command = Command::where('deploy_step_id', $deployStep_id)
            ->where('project_id', $project_id)
            ->where('custom', 1)
            ->findOrFail($id);
        $data = $request->only(['name', 'script', 'custom', 'weight']);
        $command->update($data);

        return $command;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($project_id, $deployStep_id, $id)
    {
        $command = Command::where('deploy_step_id', $deployStep_id)
            ->where('project_id', $project_id)
            ->where('custom', 1)
            ->findOrFail($id);
        $command->delete();

        return $command;
    }
}
