<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Api\DeployTarget\CreateDeployTargetRequest;
use App\Http\Requests\Api\DeployTarget\UpdateDeployTargetRequest;
use App\Models\DeployTarget;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeployTargetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($project_id)
    {
        return Project::findOrFail($project_id)->deployTargets()->with('server')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateDeployTargetRequest $request
     * @param $project_id
     * @return \Illuminate\Http\Response
     */
    public function store(CreateDeployTargetRequest $request, $project_id)
    {
        $data = $request->only(['server_id', 'user', 'path', 'automatic', 'active']);
        $project = Project::findorFail($project_id);
        $deploy_target = $project->deployTargets()->create($data);

        return $deploy_target;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($project_id, $id)
    {
        return Project::findOrFail($project_id)->deployTargets()->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateDeployTargetRequest $request
     * @param $project_id
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDeployTargetRequest $request, $project_id, $id)
    {
        $deploy_target = Project::findOrFail($project_id)->deployTargets()->findOrFail($id);
        $data = $request->only(['server_id', 'user', 'path', 'automatic', 'active']);
        $deploy_target->update($data);

        return $deploy_target;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($project_id, $id)
    {
        $deploy_target = DeployTarget::findOrFail($id);
        $deploy_target->delete();

        return $deploy_target;
    }
}
