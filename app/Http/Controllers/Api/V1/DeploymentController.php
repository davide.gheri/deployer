<?php

namespace App\Http\Controllers\Api\V1;

use App\Jobs\Queue\DeployCode;
use App\Models\Deployment;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeploymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($project_id)
    {
        return Project::findOrFail($project_id)->deployments()->orderBy('created_at', 'desc')->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $project_id)
    {
        $deploy = Deployment::create([
            'project_id' => $project_id,
            'user_id' => $request->user()->id,
            'status' => Deployment::PENDING
        ]);
        $deploy->project->update([
            'status' => Project::PENDING
        ]);
        $this->dispatch((new DeployCode($deploy))->onQueue(config('deployer.queue.deployment')));

        return $deploy;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($project_id, $id)
    {
        return Deployment::with('project.deployTargets')->where('project_id', $project_id)->findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
