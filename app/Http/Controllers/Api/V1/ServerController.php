<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Server;
use App\Services\Scripts\Process;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Server::all();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $server = Server::create($request->only(['name', 'ip_address', 'user', 'port']));
        return $server;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Server::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only(['name', 'ip_address', 'user', 'status', 'port']);
        $server = Server::findOrFail($id);
        $server->update($data);
        return $server;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $server = Server::findOrFail($id);
        $server->delete();
        return $server;
    }

    public function test(Process $process, $id)
    {
        $server = Server::findOrFail($id);
        $process->setServer($server);
        $process->setTimeout(20);
        $process->setScript('ls -a', [], false)->run();


        if(!$process->isSuccessful()) {
            return response()->json(['status' => '2', 'error' => $process->getErrorOutput()], 422);
        }
        return response()->json(['status' => '0'], 200);
    }
}
