<?php

namespace App\Http\Controllers\Webhooks;

use App\Jobs\Queue\DeployCode;
use App\Models\Deployment;
use App\Models\Project;
use App\Services\Webhooks\Gitlab;
use App\Services\Webhooks\Webhook;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectWebhookController extends Controller
{
    private $services = [
        Gitlab::class
    ];

    /**
     * @var Project
     */
    private $project;

    public function __invoke(Request $request, $id)
    {
        $this->project = Project::findOrFail($id);
        if ($this->project->deployTargets()->automatic()->count() == 0) {
            return response()->json(['success' => false]);
        }

        $payload = $this->parseRequest($request);
        if (!$payload) {
            return response()->json(['success' => false]);
        }
        $deploy = Deployment::create([
            'project_id' => $id,
            'user_id' => 0,
            'status' => Deployment::PENDING,
            'commit' => $payload['commit'] ?? '',
            'committer' => $payload['committer'] ?? '',
            'commit_message' => $payload['commit_message'] ?? ''
        ]);
        $this->project->update([
            'status' => Project::PENDING
        ]);
        $this->dispatch((new DeployCode($deploy))->onQueue(config('deployer.queue.deployment')));
    }

    private function parseRequest(Request $request)
    {
        foreach ($this->services as $service) {
            /** @var $service Webhook */
            if ($service::isOrigin($request)) {
                $payload = (new $service($request))->parse();
                if ($payload['branch'] == $this->project->branch) {
                    return $payload;
                }
            }
        }
    }
}
