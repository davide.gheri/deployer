<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\FormRequest;

class ApiRequest extends FormRequest
{
    public function user($guard = null)
    {
        return parent::user('api');
    }
}