<?php

namespace App\Http\Requests\Api\Command;

use App\Http\Requests\Api\ApiRequest;
use App\Http\Requests\Traits\HasNameField;

class CreateCommandRequest extends ApiRequest
{
    use HasNameField;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'script' => 'string',
            'weight' => 'integer',
        ];
    }
}
