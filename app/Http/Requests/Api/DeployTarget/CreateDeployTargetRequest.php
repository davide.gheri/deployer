<?php

namespace App\Http\Requests\Api\DeployTarget;

use App\Http\Requests\Api\ApiRequest;

class CreateDeployTargetRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'server_id' => 'required|string|exists:servers,id|max:255',
            'path' => 'required|string|max:255',
            'user' => 'string|max:255'
        ];
    }
}
