<?php

namespace App\Http\Requests\Api\Project;

use App\Http\Requests\Api\ApiRequest;
use App\Http\Requests\Traits\HasNameField;

class CreateProjectRequest extends ApiRequest
{
    use HasNameField;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'repository' => 'required|string',
            'branch' => 'required|string',
            'url' => 'sometimes|url'
        ];
    }
}
