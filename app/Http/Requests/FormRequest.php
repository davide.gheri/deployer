<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest as BaseRequest;
use Illuminate\Validation\Validator;

class FormRequest extends BaseRequest
{
    protected function isApiRequest()
    {
        return $this->ajax() || $this->isXmlHttpRequest();
    }

    public function user($guard = null)
    {
        if($this->isApiRequest()) {
            $guard = 'api';
        }
        return parent::user($guard);
    }

    public function withValidator(Validator $validator)
    {
        if($rules = $this->getTraitRules()) {
            $validator->addRules($rules);
        }
    }

    protected function getTraitRules()
    {
        return array_reduce(class_uses(static::class), function ($rules, $trait) {
            $rulesMethod = $this->makeRulesMethodName($trait);
            if($rulesMethod && method_exists($this, $rulesMethod)) {
                $rules = array_merge($rules, $this->{$rulesMethod}());
            }
            return $rules;
        }, []);
    }

    protected function makeRulesMethodName($trait)
    {
        preg_match('/^Has([A-Za-z]+)Field$/', class_basename($trait), $matches);

        return isset($matches[1]) ? camel_case($matches[1]).'Rules' : null;
    }
}