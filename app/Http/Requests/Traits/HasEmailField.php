<?php

namespace App\Http\Requests\Traits;

trait HasEmailField
{
    protected function nameRules()
    {
        return [
            'email' => 'required|max:255|string|email'
        ];
    }
}