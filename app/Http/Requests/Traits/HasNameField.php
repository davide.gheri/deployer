<?php

namespace App\Http\Requests\Traits;

trait HasNameField
{
    protected function nameRules()
    {
        return [
            'name' => 'required|max:255|string'
        ];
    }
}