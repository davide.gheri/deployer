<?php

namespace App\Http\Requests\Traits;

trait HasPasswordField
{
    protected function passwordRules()
    {
        return [
            'password' => 'required|max:255|string|min:6|confirmed'
        ];
    }
}