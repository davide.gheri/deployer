<?php

namespace App\Jobs;

use App\Jobs\Server\RunOnServer;

class ActivateRelease extends RunOnServer
{
    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function run()
    {
        $this->process->setServer($this->server);
        $script = $this->process->setScript('ActivateRelease', [
            'project_path' => $this->deployTarget->path,
            'release_path' => $this->deployTarget->path . '/releases/' . $this->deployment->id
        ]);
        $script->run(function($type, $buffer) {
            if ($type === 'err') {
                $this->deployment->setProgress('[ERROR] ' . $buffer, [], 'error');
            } else {
                $this->deployment->setProgress($buffer);
            }
        });
        if (!$this->process->isSuccessful()) {
            $this->deployment->setProgress(
                '[ERROR:SSH] Cannot activate the release! - %s'
                [$this->process->getErrorOutput()], 'error'
            );
            throw new \RuntimeException('Cannot activate the release - ' . $this->process->getErrorOutput());
        }
    }
}
