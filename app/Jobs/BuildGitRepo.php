<?php

namespace App\Jobs;

use App\Models\Command;
use App\Models\Deployment;
use App\Models\Project;
use App\Services\Scripts\Process;
use Illuminate\Config\Repository;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class BuildGitRepo
{
    use Dispatchable, SerializesModels;

    /**
     * @var Project
     */
    private $project;
    /**
     * @var Deployment
     */
    private $deployment;
    /**
     * @var Command
     */
    private $command;

    /**
     * Create a new job instance.
     *
     * @param Deployment $deployment
     * @param Command $command
     * @param Project null $project
     */
    public function __construct($deployment, $command, $project = null)
    {
        $this->deployment = $deployment;
        $this->command = $command;
        $this->project = $project ?? $deployment->project;
    }

    /**
     * Execute the job.
     *
     * @param Process $process
     * @param Repository $config
     * @return void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle(Process $process, Repository $config)
    {
        $wrapper_file = $config->get('deployer.ssh.wrapper');

        $script = $process->setScript('BuildGitRepo', [
            'wrapper_file' => $wrapper_file,
            'mirror_path' => $this->project->mirrorPath(),
            'repository' => $this->project->repository
        ]);

        $this->project->update(['is_mirroring' => true]);

        $script->run(function($type, $buffer) {
            if ($this->deployment instanceof Deployment) {
                $this->deployment->setProgress($buffer);
            }
        });

        $this->project->update(['is_mirroring' => false]);

        if (!$process->isSuccessful()) {
            if ($this->deployment instanceof Deployment) {
                $this->deployment->setProgress(
                    '[ERROR:GIT] Cannot build a mirror repo - %s'
                    [$process->getErrorOutput()], 'error'
                );
            }
            throw new \RuntimeException('Cannot build a mirror repo - ' . $process->getErrorOutput());
        }
    }
}
