<?php

namespace App\Jobs;

use App\Jobs\Server\RunOnServer;
use Illuminate\Support\Facades\Log;

class CopyFiles extends RunOnServer
{
    protected $shouldRun = false;

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function run()
    {
        $files = $this->getFilesToString();
        if ($this->shouldRun) {
            $this->process->setServer($this->server);
            $script = $this->process->setScript('CopyFiles', [
                'release_path' => $this->deployTarget->path . '/releases/' . $this->deployment->id,
                'files' => $files,
            ]);
            $script->run(function($type, $buffer) {
                if ($type === 'err') {
                    $this->deployment->setProgress('[ERROR] ' . $buffer, [], 'error');
                } else {
                    $this->deployment->setProgress($buffer);
                }
            });
            if (!$this->process->isSuccessful()) {
                $this->deployment->setProgress(
                    '[ERROR:SSH] Cannot copy the files! - %s',
                    [$this->process->getErrorOutput()], 'error'
                );
                throw new \RuntimeException('Cannot copy the files - ' . $this->process->getErrorOutput());
            }
        }

    }

    protected function getFilesToString()
    {
        $files = $this->deployment->project->options['files'] ?? [];
        if (!empty($files)) {
            $this->shouldRun = true;
        } else {
            $this->shouldRun = false;
        }
        $string = '';
        foreach ($files as $file) {
            $string .= '["'. $file['origin'] .'"]="' . $file['destination'] . '" ';
        }
        return $string;
    }
}
