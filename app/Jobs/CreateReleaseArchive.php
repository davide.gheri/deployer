<?php

namespace App\Jobs;

use App\Models\Deployment;
use App\Services\Scripts\Process;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Bus\Dispatchable;

class CreateReleaseArchive
{
    use Dispatchable, SerializesModels;

    protected $deployment;

    /**
     * Create a new job instance.
     *
     * @param Deployment $deployment
     */
    public function __construct(Deployment $deployment)
    {
        $this->deployment = $deployment;
    }

    /**
     * Execute the job.
     *
     * @param Process $process
     * @return void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle(Process $process)
    {
        $tmp_dir = 'clone_' . $this->deployment->project_id . '_' . $this->deployment->id;
        $tmp_dir = storage_path('app/tmp/' . $tmp_dir);
        $archive = $this->deployment->archivePath() . '.tar.gz';

        $script = $process->setScript('CreateReleaseArchive', [
            'mirror_path'            => $this->deployment->project->mirrorPath(),
            'scripts_path'           => resource_path('scripts'),
            'tmp_path'               => $tmp_dir,
            'sha'                    => $this->deployment->commit,
            'release_archive_parent' => $this->deployment->project->archivePath(),
            'release_archive'        => $archive,
        ]);

        $script->run(function($type, $buffer) {
//            if ($type == 'err') {
//                $this->deployment->setProgress('[ERROR] ' . $buffer, [], 'error');
//            } else {
//                $this->deployment->setProgress($buffer);
//            }
        });
        if (!$process->isSuccessful()) {
            $this->deployment->setProgress(
                '[ERROR:GIT] Cannot create release archive - %s',
                [$process->getErrorOutput()], 'error'
            );
            throw new \RuntimeException('Cannot not create release archive - ' . $process->getErrorOutput());
        }

        $this->deployment->update(['archive' => $archive]);
    }
}
