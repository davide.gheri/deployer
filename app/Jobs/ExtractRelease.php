<?php

namespace App\Jobs;

use App\Jobs\Server\RunOnServer;

class ExtractRelease extends RunOnServer
{
    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function run()
    {
        $this->process->setServer($this->server);
        $script = $this->process->setScript('ExtractRelease', array_merge($this->server_data, [
            'project_path' => $this->deployTarget->path,
            'releases_path' => $this->deployTarget->path . '/releases/',
            'release_path' => $this->deployTarget->path . '/releases/' . $this->deployment->id,
            'remote_archive' => $this->remote_file
        ]));

        $script->run(function($type, $buffer) {
            if ($type === 'err') {
                $this->deployment->setProgress('[ERROR] ' . $buffer, [], 'error');
            } else {
                $this->deployment->setProgress($buffer);
            }
        });
        if (!$this->process->isSuccessful()) {
            $this->deployment->setProgress(
                '[ERROR:SSH] Cannot Extract the release archive! - %s'
                [$this->process->getErrorOutput()], 'error'
            );
            throw new \RuntimeException('Cannot Extract the release archive - ' . $this->process->getErrorOutput());
        }
    }
}
