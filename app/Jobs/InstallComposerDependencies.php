<?php

namespace App\Jobs;

use App\Jobs\Server\RunOnServer;
use Illuminate\Support\Facades\Log;

class InstallComposerDependencies extends RunOnServer
{
    /**
     * Execute the job.
     *
     * @return void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function run()
    {
        $this->process->setServer($this->server);
        $script = $this->process->setScript('InstallComposerDependencies', array_merge($this->server_data, [
            'project_path' => $this->deployTarget->path,
            'release_path' => $this->deployTarget->path . '/releases/' . $this->deployment->id,
            'include_dev'  => false
        ]));

        $script->run(function($type, $buffer) {
            if ($type === 'err') {
                $this->deployment->setProgress('[INFO] ' . $buffer, [], 'warning');
            } else {
                $this->deployment->setProgress($buffer);
            }
        });
        if (!$this->process->isSuccessful()) {
            $this->deployment->setProgress(
                '[ERROR:SSH] Cannot Install composer dependencies! - %s'
                [$this->process->getErrorOutput()], 'error'
            );
            throw new \RuntimeException('Cannot Install composer dependencies - ' . $this->process->getErrorOutput());
        }
    }
}
