<?php

namespace App\Jobs;

use App\Jobs\Server\RunOnServer;
use Illuminate\Support\Facades\Log;

class LinkSharedFolders extends RunOnServer
{
    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function run()
    {
        $shareds = $this->getShareds();
        $shared_dir = $this->deployTarget->path . '/shared';
        $release_dir = $this->deployTarget->path . '/releases/' . $this->deployment->id;
        $this->process->setServer($this->server);
        foreach ($shareds as $shared) {
            $folder = $shared['path'];
            $script = $this->process->setScript('ShareFolder', [
                'shared_dir' => $shared_dir,
                'release_dir' => $release_dir,
                'folder' => $folder
            ]);
            $script->run(function($type, $buffer) {
                if ($type === 'err') {
                    $this->deployment->setProgress('[ERROR] ' . $buffer, [], 'error');
                } else {
                    $this->deployment->setProgress($buffer);
                }
            });
            if (!$script->isSuccessful()) {
                $this->deployment->setProgress(
                    '[ERROR:SSH] Cannot link the shared folder %s! - %s',
                    [$folder, $this->process->getErrorOutput()], 'error'
                );
            }
        }
    }

    protected function getShareds()
    {
        $shareds = $this->deployment->project->options['shareds'] ?? [];
        return $shareds;
    }
}
