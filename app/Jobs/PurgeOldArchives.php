<?php

namespace App\Jobs;

use App\Models\Command;
use App\Models\Deployment;
use App\Models\DeployTarget;
use App\Models\Project;
use Illuminate\Config\Repository;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Bus\Dispatchable;

class PurgeOldArchives
{
    use Dispatchable, SerializesModels;
    /**
     * @var Deployment
     */
    private $deployment;
    /**
     * @var Command
     */
    private $command;
    /**
     * @var DeployTarget
     */
    private $deployTarget;
    /**
     * @var Project
     */
    private $project;

    /**
     * Create a new job instance.
     *
     * @param Deployment $deployment
     * @param Command $command
     * @param DeployTarget $deployTarget
     * @param null $project
     */
    public function __construct($deployment, $command, $deployTarget, $project = null)
    {
        $this->deployment = $deployment;
        $this->command = $command;
        $this->deployTarget = $deployTarget;
        $this->project = $project ?? $deployment->project;
    }

    /**
     * Execute the job.
     *
     * @param Repository $config
     * @param Filesystem $filesystem
     * @return void
     */
    public function handle(Repository $config, Filesystem $filesystem)
    {
        $to_keep = $config->get('deployer.releases_to_keep');

        if ($this->deployment instanceof Deployment) {
            $this->deployment->setProgress('[LOCAL:CLEAN] Removing old local archives... keeping ' . $to_keep);
        }

        $archive_path = $this->project->relativeArchivePath();
        $to_keep = $this->project->deployments()->lasts($to_keep)
            ->get()->map(function($deployment) {
                return preg_replace('/^' . preg_quote(storage_path('app/'), '/') . '/', '', $deployment->archivePath()) . '.tar.gz';
            });
        $toRemove = collect($filesystem->files($archive_path))->diff($to_keep);
        $filesystem->delete($toRemove->toArray());

        if ($this->deployment instanceof Deployment) {
            $this->deployment->setProgress('[LOCAL:CLEAN] Done.');
        }

    }
}
