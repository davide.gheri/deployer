<?php

namespace App\Jobs;

use App\Jobs\Server\RunOnServer;

class PurgeOldReleases extends RunOnServer
{
    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function run()
    {
        $to_keep = $this->config->get('deployer.releases_to_keep');
        $this->process->setServer($this->server);
        $script = $this->process->setScript('PurgeOldReleases', array_merge($this->server_data, [
            'releases_path' => $this->deployTarget->path . '/releases/',
            'to_keep' => $to_keep
        ]));

        $script->run(function($type, $buffer) {
            if ($type === 'err') {
                $this->deployment->setProgress('[ERROR] ' . $buffer, [], 'error');
            } else {
                $this->deployment->setProgress($buffer);
            }
        });
        if (!$this->process->isSuccessful()) {
            $this->deployment->setProgress(
                '[ERROR:SSH] Cannot remove old releases! - %s'
                [$this->process->getErrorOutput()], 'error'
            );
            throw new \RuntimeException('Cannot remove old releases - ' . $this->process->getErrorOutput());
        }
    }
}
