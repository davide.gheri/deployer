<?php

namespace App\Jobs\Queue;

use App\Jobs\RunCustomCommand;
use App\Models\Deployment;
use App\Models\Project;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

abstract class Deploy implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels, DispatchesJobs;

    public $tries = 1;
    public $timeout = 1000;

    public $deployment;

    /**
     * Create a new job instance.
     *
     * @param Deployment $deployment
     */
    public function __construct(Deployment $deployment)
    {
        $this->deployment = $deployment;
    }

    public function failed($e)
    {
        $this->deployment->setProgress('[ERROR:FAILED] Deploy failed', [], 'error');
        $this->deployment->update([
            'status' => Deployment::FAILED,
            'finished_at' => Carbon::now()
        ]);
        $this->deployment->project->update([
            'status' => Project::FAILED,
            'last_run' => Carbon::now()
        ]);
    }

    protected function runSteps(int $step_id, $model = null)
    {
        if ($step = $this->deployment->project->deploySteps()->getStep($step_id)->first()) {
            $commands = $step->commands()
                ->where(function($q) {
                    $q->whereNull('project_id')
                        ->orWhere('project_id', $this->deployment->project_id);
                })->get();
            foreach ($commands as $command) {
                if (!$command->custom) {
                    $this->runCommandJob($command, $model);
                } else {
                    $this->dispatch(new RunCustomCommand($this->deployment, $command, $model));
                }
            }
        }
    }

    protected function runCommandJob($command, $model = null)
    {
        $job = '\App\\Jobs\\' . $command->script;
        if (class_exists($job)) {
            $this->dispatch(new $job($this->deployment, $command, $model));
        }
    }
}