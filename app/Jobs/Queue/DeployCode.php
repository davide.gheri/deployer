<?php

namespace App\Jobs\Queue;

use App\Models\Deployment;
use App\Models\DeployStep;
use App\Models\Project;
use Carbon\Carbon;

class DeployCode extends Deploy
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->waitIfProjectMirroring();

        $this->deployment->update([
            'status' => Deployment::DEPLOYING,
            'started_at' => Carbon::now()
        ]);
        $this->deployment->project->update([
            'status' => Project::DEPLOYING
        ]);

        $this->runCloneStep();
        $this->runReleaseArchiveStep();

        $this->dispatch(new DeployOnServer($this->deployment));
    }

    protected function runCloneStep()
    {
        return $this->runSteps(DeployStep::CLONE);
    }

    protected function runReleaseArchiveStep()
    {
        return $this->runSteps(DeployStep::ARCHIVE);
    }

    protected function waitIfProjectMirroring()
    {
        $is_mirroring = $this->deployment->project->is_mirroring;
        while ($is_mirroring) {
            sleep(3);
            $project = $this->deployment->project->fresh();
            $is_mirroring = $project->is_mirroring;
        }
    }

    public function tags()
    {
        return ['deployCode_local'];
    }
}
