<?php

namespace App\Jobs\Queue;

use App\Models\Deployment;
use App\Models\DeployStep;
use App\Models\Project;
use Carbon\Carbon;

class DeployOnServer extends Deploy
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->loopDeployTargets();

        $this->deployment->setProgress("\n" . '[COMPLETE] Deploy completed with SUCCESS' . "\n", [], 'success');
        $this->deployment->update([
            'status' => Deployment::COMPLETED,
            'finished_at' => Carbon::now()
        ]);
        $this->deployment->project->update([
            'status' => Project::FINISHED,
            'last_run' => Carbon::now()
        ]);
    }

    protected function loopDeployTargets()
    {
        $deployTargets = $this->deployment->project->deployTargets()->active()
            ->when($this->deployment->isAutomatic(), function($query) {
                $query->automatic();
            })->get();
        $deployTargets->each(function($deployTarget) {
            $this->runSendFileToServerStep($deployTarget);
            $this->runInstallDependenciesStep($deployTarget);
            $this->runActivateStep($deployTarget);
            $this->runCleanStep($deployTarget);
        });
    }

    protected function runSendFileToServerStep($deployTarget)
    {
        return $this->runSteps(DeployStep::SEND, $deployTarget);
    }

    protected function runInstallDependenciesStep($deployTarget)
    {
        return $this->runSteps(DeployStep::INSTALL, $deployTarget);
    }

    protected function runActivateStep($deployTarget)
    {
        return $this->runSteps(DeployStep::ACTIVATE, $deployTarget);
    }

    protected function runCleanStep($deployTarget)
    {
        return $this->runSteps(DeployStep::CLEAN, $deployTarget);
    }

    public function tags()
    {
        return ['deployCode_server'];
    }
}