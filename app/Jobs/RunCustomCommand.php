<?php

namespace App\Jobs;

use App\Models\Command;
use App\Models\Deployment;
use App\Models\DeployStep;
use App\Models\DeployTarget;
use App\Services\Scripts\Process;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Bus\Dispatchable;

class RunCustomCommand
{
    use Dispatchable, SerializesModels;

    protected $deployment;
    protected $command;
    protected $deployTarget;
    /** @var Process $process */
    protected $process;
    protected $server;

    /**
     * Create a new job instance.
     *
     * @param Deployment $deployment
     * @param Command $command
     * @param DeployTarget $deployTarget
     */
    public function __construct(Deployment $deployment, Command $command, DeployTarget $deployTarget)
    {
        $this->deployment = $deployment;
        $this->command = $command;
        $this->deployTarget = $deployTarget;
    }

    /**
     * Execute the job.
     *
     * @param Process $process
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle(Process $process)
    {
        $this->process = $process;
        $this->server = $this->deployTarget->server;
        switch ($this->command->deployStep->step) {
            case DeployStep::CLONE:
            case DeployStep::ARCHIVE:
                $this->runLocally();
            case DeployStep::SEND:
            case DeployStep::INSTALL:
            case DeployStep::ACTIVATE:
            case DeployStep::CLEAN:
                $this->runOnServer();
        }
    }

    protected function runLocally()
    {
        $this->deployment->setProgress('[Local:Custom] Running custom command: ' . $this->command->name);
        //TODO Meanings of running local custom scripts?
    }

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function runOnServer()
    {
        $release_path = $this->deployTarget->path . '/releases/' . $this->deployment->id;

        $this->process->setServer($this->server);
        $this->deployment->setProgress('[SSH:Custom] Running custom command: ' . $this->command->name);
        $script = $this->process->setScript($this->command->script, [], Process::DIRECT_INPUT);
        $script->prependScript('cd ' . $release_path);

        $this->runScript($script);
    }

    /**
     * @param $script
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function runScript(Process $script)
    {
        $script->run(function($type, $buffer) {
            if ($type === 'err') {
                $this->deployment->setProgress('[ERROR] ' . $buffer, [], 'error');
            } else {
                $this->deployment->setProgress($buffer);
            }
        });
        if (!$this->process->isSuccessful()) {
            $this->deployment->setProgress(
                '[ERROR] Cannot run custom command! - %s: %s',
                [$this->command->name, $this->process->getErrorOutput()], 'error'
            );
            throw new \RuntimeException('Cannot run custom command - ' . $this->command->name . ': ' . $this->process->getErrorOutput());
        }
    }
}
