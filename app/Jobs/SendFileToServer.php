<?php

namespace App\Jobs;

use App\Jobs\Server\RunOnServer;

class SendFileToServer extends RunOnServer
{
    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function run()
    {
        if (!$this->testArchive()) {
            $this->deployment->setProgress('[ERROR:SSH] Cannot find the release archive file!', [], 'error');
            throw new \RuntimeException('Cannot send over SSH - cannot find the release archive file');
        }

        $script = $this->process->setScript('SendFileToServer', array_merge($this->server_data, [
            'deploy_target' => $this->deployTarget->server->ip_address . ' - ' . $this->deployTarget->path,
            'local_file' => $this->deployment->archive,
            'remote_file' => $this->remote_file,
        ]));

        $script->run(function($type, $buffer) {
            if ($type === 'err') {
                $this->deployment->setProgress('[ERROR] ' . $buffer, [], 'error');
            } else {
                $this->deployment->setProgress($buffer);
            }
        });
        if (!$this->process->isSuccessful()) {
            $this->deployment->setProgress(
                '[ERROR:SSH] Cannot send archive over SSH - %s'
                [$this->process->getErrorOutput()], 'error'
            );
            throw new \RuntimeException('Cannot send archive over SSH - ' . $this->process->getErrorOutput());
        }
    }

    protected function testArchive()
    {
        $archive = $this->deployment->archive;

        return $this->filesystem->exists($archive);
    }

}
