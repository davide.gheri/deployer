<?php

namespace App\Jobs\Server;

use App\Models\Command;
use App\Models\Deployment;
use App\Models\DeployTarget;
use App\Services\Scripts\Process;
use Illuminate\Config\Repository;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Bus\Dispatchable;

abstract class RunOnServer
{
    use Dispatchable, SerializesModels;

    protected $deployment;
    protected $command;
    protected $deployTarget;
    protected $remote_file;
    protected $server;
    protected $server_data;
    /** @var Process $process */
    protected $process;
    /** @var Repository $config */
    protected $config;
    /** @var Filesystem $filesystem */
    protected $filesystem;

    /**
     * Create a new job instance.
     *
     * @param Deployment $deployment
     * @param Command $command
     * @param DeployTarget $deployTarget
     */
    public function __construct(Deployment $deployment, Command $command, DeployTarget $deployTarget)
    {
        $this->deployment = $deployment;
        $this->command = $command;
        $this->deployTarget = $deployTarget;

        $this->remote_file = $this->deployTarget->path . '/' . $this->deployment->id . '.tar.gz';
    }

    /**
     * Execute the job.
     *
     * @param Process $process
     * @param Repository $config
     * @param Filesystem $filesystem
     * @return void
     */
    public function handle(Process $process, Repository $config, Filesystem $filesystem)
    {
        $this->process = $process;
        $this->config = $config;
        $this->filesystem = $filesystem;
        $this->server = $this->deployTarget->server;

        $this->server_data = $this->getServerData($this->server);

        $this->run();
    }

    abstract protected function run();

    protected function getServerData($server)
    {
        return [
            'user' => $this->deployTarget->user,
            'ip_address' => $server->ip_address,
            'port' => $server->port,
            'private_key' => $this->config->get('deployer.ssh.id_rsa')
        ];
    }
}