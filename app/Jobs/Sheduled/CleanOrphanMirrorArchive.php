<?php

namespace App\Jobs\Sheduled;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Foundation\Testing\Concerns\InteractsWithConsole;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CleanOrphanMirrorArchive implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, InteractsWithConsole;

    public $tries = 3;
    public $timeout = 60;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->artisan('deployer:orphans-clean');
    }
}
