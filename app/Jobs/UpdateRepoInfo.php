<?php

namespace App\Jobs;

use App\Models\Command;
use App\Models\Deployment;
use App\Services\Scripts\Process;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class UpdateRepoInfo
{
    use Dispatchable, SerializesModels;

    protected $deployment;
    public $command;

    /**
     * Create a new job instance.
     *
     * @param Deployment $deployment
     * @param Command $command
     */
    public function __construct(Deployment $deployment, Command $command)
    {
        $this->deployment = $deployment;
        $this->command = $command;
    }

    /**
     * Execute the job.
     *
     * @param Process $process
     * @return void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle(Process $process)
    {
        $script = $process->setScript('UpdateRepoInfo', [
            'mirror_path' => $this->deployment->project->mirrorPath(),
            'git_ref' => $this->deployment->branch
        ]);

        $script->run(function($type, $buffer) {
            $this->deployment->setProgress($buffer);
        });
        if (!$process->isSuccessful()) {
            $this->deployment->setProgress(
                '[ERROR:GIT] Cannot get repository info - %s',
                [$process->getErrorOutput()], 'error'
            );
            throw new \RuntimeException('Cannot get repository info - ' . $process->getErrorOutput());
        }

        list($commit, $committer, $email, $message) = explode("\x09", $process->getOutput());
        $this->deployment->update([
            'commit' => trim($commit),
            'committer' => trim($committer) . ' <' . trim($email) . '>',
            'commit_message' => trim($message)
        ]);
    }
}
