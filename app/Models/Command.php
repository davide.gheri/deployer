<?php

namespace App\Models;

use App\Models\Traits\HasUuidKey;
use Illuminate\Database\Eloquent\Model;

class Command extends Model
{
    use HasUuidKey;

    public $incrementing = false;

    protected $fillable = [
        'name', 'script', 'deploy_stage_id', 'custom', 'weight', 'project_id'
    ];

    protected $casts = [
        'custom' => 'boolean'
    ];

    public function deployStep()
    {
        return $this->belongsTo(DeployStep::class);
    }
}
