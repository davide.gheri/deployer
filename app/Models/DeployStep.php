<?php

namespace App\Models;

use App\Models\Traits\HasUuidKey;
use Illuminate\Database\Eloquent\Model;

class DeployStep extends Model
{
    use HasUuidKey;

    const CLONE = 0;
    const ARCHIVE = 1;
    const SEND = 2;
    const INSTALL = 3;
    const ACTIVATE = 4;
    const CLEAN = 5;

    public $incrementing = false;

    protected $fillable = [
        'name', 'step', 'description'
    ];

//    protected $with = ['commands'];

    public function projects()
    {
        return $this->belongsTo(Project::class);
    }

    public function commands()
    {
        return $this->hasMany(Command::class)->orderBy('weight', 'asc');
    }

    public function scopeGetStep($query, $step)
    {
        return $query->whereStep($step);
    }

}
