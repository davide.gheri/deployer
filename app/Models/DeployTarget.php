<?php

namespace App\Models;

use App\Models\Traits\HasUuidKey;
use Illuminate\Database\Eloquent\Model;

class DeployTarget extends Model
{
    use HasUuidKey;

    public $incrementing = false;

    protected $with = ['server', 'project'];
    protected $appends = ['user'];

    protected $guarded = [];

    protected $casts = [
        'active' => 'boolean',
        'automatic' => 'boolean'
    ];

    public function getUserAttribute($value)
    {
        return $value ?? $this->server->user;
    }

    public function server()
    {
        return $this->belongsTo(Server::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function scopeActive($query)
    {
        return $query->whereActive(1);
    }

    public function scopeAutomatic($query)
    {
        return $query->whereAutomatic(1);
    }
}
