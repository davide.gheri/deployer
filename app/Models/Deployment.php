<?php

namespace App\Models;

use App\Models\Traits\HasUuidKey;
use Illuminate\Database\Eloquent\Model;

class Deployment extends Model
{
    use HasUuidKey;

    public $incrementing = false;

    const COMPLETED             = 0;
    const PENDING               = 1;
    const DEPLOYING             = 2;
    const FAILED                = 3;
    const COMPLETED_WITH_ERRORS = 4;
    const ABORTING              = 5;
    const ABORTED               = 6;
    const LOADING               = 'Loading';

    const CHANNEL = 'deployment';

    protected $dates = ['started_at', 'finished_at'];

    protected $guarded = [];

    protected $appends = ['branch'];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function getBranchAttribute($value)
    {
        return $value ?: $this->project->branch;
    }

    public function archivePath()
    {
        return $this->project->archivePath() . $this->project->branch . '_' . $this->id;
    }

    public function setProgress(string $string, array $replace = [], $level = null)
    {
        if (!$level) {
            $level = 'black';
        }
        if ($level == 'error') {
            $level = 'danger';
        }

        $class = $level;
        $string = '<span class="text-' . $class . '">'. @vsprintf($string, $replace) .'</span>';
        $progress = $this->progress . $string . "\n";
        $this->update(['progress' => $progress, 'finished_at' => null]);
    }

    public function scopeDone($query)
    {
        return $query->whereNotNull('started_at')
            ->whereNotNull('finished_at');
    }

    public function scopeLasts($query, $limit)
    {
        return $query->done()->orderBy('finished_at', 'desc')
            ->limit($limit);
    }

    public function isAutomatic()
    {
        return false; //TODO
    }
}
