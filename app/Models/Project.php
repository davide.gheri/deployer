<?php

namespace App\Models;

use Creativeorange\Gravatar\Facades\Gravatar;
use App\Models\Traits\HasUuidKey;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasUuidKey;

    const FINISHED     = 0;
    const PENDING      = 1;
    const DEPLOYING    = 2;
    const FAILED       = 3;
    const NOT_DEPLOYED = 4;

    const CHANNEL = 'project';

    public $incrementing = false;

    protected $fillable = [
        'id',
        'name',
        'repository',
        'branch',
        'url',
        'last_run',
        'status',
        'options',
        'is_mirroring'
    ];

    protected $appends = ['repository_path', 'repository_url', 'vcs_url', 'avatar', 'success_rate'];

    protected $casts = [
        'options' => 'array',
        'is_mirroring' => 'boolean'
    ];

    public static function boot()
    {
        parent::boot();

        static::created(function(Project $project) {
            $deploySteps = DeployStep::whereIn('step', config('deployer.default_steps'))->get()->pluck('id')->toArray();
            $project->deploySteps()->attach($deploySteps);
        });
    }

    public function deployments()
    {
        return $this->hasMany(Deployment::class);
    }

    public function deployTargets()
    {
        return $this->hasMany(DeployTarget::class);
    }

    public function deploySteps()
    {
        return $this->belongsToMany(DeployStep::class, 'deploy_step_project');
    }

    public function scopeNotDeploying($query)
    {
        return $query->whereNotIn('status', [Project::PENDING, Project::DEPLOYING]);
    }

    /**
     * Parses the repository URL to get the user, domain, port and path parts.
     *
     * @return array
     */
    public function accessDetails()
    {
        $info = [];
        if (preg_match('#^(.+)://(.+)@(.+):([0-9]*)\/?(.+)\.git$#', $this->repository, $matches)) {
            $info['scheme']    = strtolower($matches[1]);
            $info['user']      = $matches[2];
            $info['domain']    = $matches[3];
            $info['port']      = $matches[4];
            $info['reference'] = $matches[5];
        } elseif (preg_match('#^(.+)@(.+):([0-9]*)\/?(.+)\.git$#', $this->repository, $matches)) {
            $info['scheme']    = 'git';
            $info['user']      = $matches[1];
            $info['domain']    = $matches[2];
            $info['port']      = $matches[3];
            $info['reference'] = $matches[4];
        } elseif (preg_match('#^https?://#i', $this->repository)) {
            $data = parse_url($this->repository);
            if (!$data) {
                return $info;
            }
            $info['scheme']    = strtolower($data['scheme']);
            $info['user']      = isset($data['user']) ? $data['user'] : '';
            $info['domain']    = $data['host'];
            $info['port']      = isset($data['port']) ? $data['port'] : '';
            $info['reference'] = substr($data['path'], 1, -4);
        }
        return $info;
    }
    /**
     * Gets the repository path.
     *
     * @return string|false
     *
     * @see Project::accessDetails()
     */
    public function getRepositoryPathAttribute()
    {
        $info = $this->accessDetails();
        if (isset($info['reference'])) {
            return $info['reference'];
        }
        return false;
    }
    /**
     * Gets the HTTP URL to the repository.
     *
     * @return string|false
     *
     * @see Project::accessDetails()
     */
    public function getRepositoryUrlAttribute()
    {
        return $this->getVcsUrlAttribute() . $this->getRepositoryPathAttribute();
    }
    /**
     * Gets the HTTP URL to the Vcs.
     *
     * @return string|false
     *
     * @see Project::accessDetails()
     */
    public function getVcsUrlAttribute()
    {
        $info = $this->accessDetails();
        if (isset($info['domain']) && isset($info['reference'])) {
            if (!isset($info['scheme']) || !starts_with($info['scheme'], 'http')) {
                $info['scheme'] = 'http';
            }
            // Always serve github links over HTTPS
            if (ends_with($info['domain'], 'github.com')) {
                $info['scheme'] = 'https';
            }
            return $info['scheme'] . '://' . $info['domain'] . '/';
        }
        return false;
    }

    public function getAvatarAttribute()
    {
        return Gravatar::get($this->id . '@none.com');
    }

    public function getSuccessRateAttribute()
    {
        $deployments = $this->deployments()->done()->whereStatus(Deployment::COMPLETED)->count();
        $total = $this->deployments()->done()->count();
        if($total > 0) {
            return (int) ceil(($deployments*100)/$total);
        }
    }

    public function mirrorPath()
    {
        return storage_path('app/' . $this->relativeMirrorPath());
    }

    public function archivePath()
    {
        return storage_path('app/' . $this->relativeArchivePath()) . '/';
    }

    public function relativeMirrorPath()
    {
        return 'mirrors/' . $this->id . '_' . $this->branch;
    }

    public function relativeArchivePath()
    {
        return 'archives/' . $this->id;
    }
}
