<?php

namespace App\Models;

use App\Models\Traits\HasUuidKey;
use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    use HasUuidKey;

    public $incrementing = false;
    protected $guarded = [];

    const SUCCESSFUL = 0;
    const UNTESTED   = 1;
    const FAILED     = 2;
    const TESTING    = 3;

}
