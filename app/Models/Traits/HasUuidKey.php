<?php

namespace App\Models\Traits;

use Ramsey\Uuid\Uuid;

trait HasUuidKey
{
    public static function bootHasUuidKey()
    {
        static::creating(function($model) {
            $model->id = Uuid::uuid1()->toString();
        });
    }
}