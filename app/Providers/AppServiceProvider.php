<?php

namespace App\Providers;

use App\Events\Observers\ModelObserver;
use App\Models\Deployment;
use App\Models\Project;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Laravel\Horizon\Horizon;

class AppServiceProvider extends ServiceProvider
{
    protected $to_observe = [
        Project::class,
        Deployment::class
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerModelObservers();

        Horizon::auth(function (Request $request) {
            return $request->user();
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    protected function registerModelObservers()
    {
        foreach ($this->to_observe as $model) {
            /** @var $model Model */
            $model::observe(ModelObserver::class);
        }
    }
}
