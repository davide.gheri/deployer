<?php

namespace App\Providers;

use App\Services\Scripts\Parser;
use App\Services\Scripts\Process;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class ProcessServiceProvider extends ServiceProvider
{

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Parser::class, function(Application $app) {
            return new Parser($app->make('files'));
        });
        $this->app->bind(Process::class, function(Application $app) {
            $process = new \Symfony\Component\Process\Process('');
            $process->setTimeout(null);
            $parser = $app->make(Parser::class);
            $logger = $app->make('log');

            return new Process($parser, $process, $logger);
        });
    }
}
