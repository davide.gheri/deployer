<?php

namespace App\Services;

use Illuminate\Log\LogManager;

class Logger extends LogManager
{
    public function info($message, array $context = [])
    {

        return $this->driver()->info($message, $context);
    }
}