<?php

namespace App\Services\Webhooks;

use Illuminate\Http\Request;

class Gitlab extends Webhook
{
    public static function isOrigin(Request $request)
    {
        return $request->headers->has('X-Gitlab-Event');
    }

    public function parse()
    {
        if (strpos($this->request->header('X-Gitlab-Event'), 'Push Hook') === false) {
            return false;
        }
        $payload = $this->request->json();
        $head = collect($payload->get('commits'))->sortByDesc(function ($commit) {
            return strtotime($commit['timestamp']);
        })->first();
        $branch = preg_replace('#refs/(tags|heads)/#', '', $payload->get('ref'));
        return [
            'branch'          => $branch,
            'source'          => 'Gitlab',
            'commit'          => $head['id'],
            'committer'       => $head['author']['name'] . '<' . $head['author']['email'] . '>',
            'commit_message'  => $head['message']
        ];
    }
}