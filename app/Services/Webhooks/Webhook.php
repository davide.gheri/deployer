<?php

namespace App\Services\Webhooks;

use Illuminate\Http\Request;

abstract class Webhook
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * Webhook constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    abstract public static function isOrigin(Request $request);

    abstract public function parse();
}