<?php

return [
    'ssh' => [
        'fingerprint' => env('SSH_FINGERPRINT'),
        'id_rsa' => storage_path('app/ssh/id_rsa'),
        'wrapper' => resource_path('scripts/GitSSHWrapper.sh')
    ],
    'queue' => [
        'deployment' => 'deployment'
    ],
    'default_steps' => [
        \App\Models\DeployStep::CLONE,
        \App\Models\DeployStep::ARCHIVE,
        \App\Models\DeployStep::SEND,
        \App\Models\DeployStep::INSTALL,
        \App\Models\DeployStep::ACTIVATE,
        \App\Models\DeployStep::CLEAN
    ],
    'releases_to_keep' => 5
];