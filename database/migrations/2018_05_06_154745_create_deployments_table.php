<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeploymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deployments', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('committer')->nullable();
            $table->string('commit')->nullable();
            $table->string('commit_message')->nullable();
            $table->string('branch')->nullable();
            $table->uuid('project_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->integer('status');
            $table->text('progress')->nullable();
            $table->string('archive')->nullable();
            $table->dateTime('started_at')->nullable();
            $table->dateTime('finished_at')->nullable();
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users');

            $table->primary('id');
            $table->index('project_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deployments');
    }
}
