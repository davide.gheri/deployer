<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Server;

class CreateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('name');
            $table->ipAddress('ip_address');
            $table->integer('port')->default(22);
            $table->string('user');
            $table->enum('status', [Server::SUCCESSFUL, Server::TESTING,
                Server::FAILED, Server::UNTESTED, ])->default(Server::UNTESTED);
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
}
