<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeployStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deploy_steps', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->string('name');
            $table->string('description')->nullable();
            $table->integer('step')->unique();
            $table->timestamps();
            $table->uuid('project_id')->nullable();

            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');

            $table->primary('id');
        });

        Schema::create('deploy_step_project', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('project_id')->index();
            $table->uuid('deploy_step_id')->index();
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('deploy_step_id')->references('id')->on('deploy_steps')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deploy_step_project');
        Schema::dropIfExists('deploy_steps');
    }
}
