<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commands', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->text('script')->nullable();
            $table->boolean('custom')->default(1);
            $table->uuid('deploy_step_id')->index();
            $table->integer('weight')->default(999);
            $table->uuid('project_id')->nullable();
            $table->timestamps();

            $table->foreign('deploy_step_id')->references('id')->on('deploy_steps')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commands');
    }
}
