import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxAdminLteModule } from 'ngx-admin-lte';

@NgModule({
  imports: [
    NgxAdminLteModule
  ],
  exports: [
    NgxAdminLteModule
  ],
  declarations: [],
})
export class AppAdminLteModule { }
