import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './modules/auth/guards/auth.guard';
import { LoginComponent } from './modules/auth/components/login/login.component';
import { UsersComponent } from './modules/users/components/users/users.component';
import { UserComponent } from './modules/users/components/user/user.component';
import { DashboardComponent } from './modules/dashboard/components/dashboard/dashboard.component';
import { UserResolve } from './modules/users/user.resolve';
import { LayoutAuthComponent, LayoutLoginComponent } from 'ngx-admin-lte';
import { ProjectListComponent } from './modules/projects/components/project-list/project-list.component';
import { ProjectFormComponent } from './modules/projects/components/project-form/project-form.component';
import { ProjectResolve } from './modules/projects/project.resolve';
import { ProjectComponent } from './modules/projects/components/project/project.component';
import { ServersListComponent } from './modules/servers/components/servers-list/servers-list.component';
import { ServerFormComponent } from './modules/servers/components/server-form/server-form.component';
import { ServerResolve } from './modules/servers/server.resolve';
import { DeploymentComponent } from './modules/projects/components/deployment/deployment.component';
import { CommandsFormListComponent } from './modules/commands/components/commands-form-list/commands-form-list.component';
import { NotFoundComponent } from './modules/core/components/not-found/not-found.component';

const routes: Routes = [
  {path: 'login', component: LayoutLoginComponent, children: [
      {path: '', component: LoginComponent}
  ]},
  {path: '', component: LayoutAuthComponent, canActivate: [AuthGuard], children:
    [
      {
        path: 'users', data: {title: 'Users'}, children: [
          {path: 'create', component: UserComponent, data: {title: 'Add User'}},
          {path: ':id', component: UserComponent, data: {title: 'Edit User'}, resolve: {user: UserResolve}},
          {path: '', component: UsersComponent, data: {title: 'Users'}},
        ]
      },
      {
        path: 'projects', data: {title: 'Projects'}, children: [
          {path: 'create', component: ProjectFormComponent, data: {title: 'Add Project'}},
          {path: ':id', resolve: {project: ProjectResolve}, children: [
              {path: '', component: ProjectComponent, data: {title: 'Project'}},
              {path: 'edit', component: ProjectFormComponent, data: {title: 'Edit project'}},
              {path: 'deployments/:deploy_id', component: DeploymentComponent, data: {title: 'Project deploy'}},
              {path: 'deploySteps/:deploy_step_id/commands', component: CommandsFormListComponent, data: {title: 'Step commands'}},
          ]},
          {path: '', component: ProjectListComponent, data: {title: 'Projects'}},
          {path: ':id/deployments', redirectTo: ':id', pathMatch: 'full'},
          {path: ':id/deploySteps', redirectTo: ':id', pathMatch: 'full'},
        ]
      },
      {
        path: 'servers', data: {title: 'Servers'}, children: [
          {path: 'create', component: ServerFormComponent, data: {title: 'Add Server'}},
          {path: ':id', component: ServerFormComponent, data: {title: 'Edit server'}, resolve: {server: ServerResolve}},
          {path: '', component: ServersListComponent, data: {title: 'Servers'}}
        ]
      },
      {path: '', component: DashboardComponent, data: {title: 'Dashboard'}},
      {path: '404', component: NotFoundComponent},
      {path: '**', redirectTo: '/404', pathMatch: 'full'}
    ],
    data: [
      {
        skin: 'skin-blue',
        display_control: false,
        display_tasks: false,
        display_messages: false,
        display_notifications: true,
        display_menu_search: false,
        display_logout: true,
        boxed_style: false,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
