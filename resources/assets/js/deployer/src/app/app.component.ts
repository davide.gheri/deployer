import { Component, OnInit } from '@angular/core';
import { StaticService } from './modules/core/services/static.service';
import { Info } from './modules/core/models/info';
import { FooterService, LogoService, MenuService, User as AdminUser, UserService } from 'ngx-admin-lte';
import { AuthService } from './modules/auth/services/auth.service';
import { Router } from '@angular/router';
import { ProjectsSidenavComponent } from './modules/projects/components/projects-sidenav/projects-sidenav.component';
import { ServersSidenavComponent } from './modules/servers/components/servers-sidenav/servers-sidenav.component';
import { EchoService } from './modules/core/services/echo.service';
import { DeploymentsService } from './modules/projects/services/deployments.service';
import { EchoEvent } from './models/echo-event';
import { ProjectsService } from './modules/projects/services/projects.service';
import { NgProgress } from '@ngx-progressbar/core';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public ready: boolean;
  private footer = {
    left_part: `<strong>
        Copyright &copy; ${new Date().getFullYear()}
        <a href="http://www.davidegheri.com" >Davidegheri</a>.
    	</strong>`,
    right_part: 'Deployer',
  };

  private logo = {
    html_mini: 'DP',
    html_lg: '<b>Deployer</b>',
  };

  private menu = [
    {
      header: 'Navigation',
    },
    {
      title: 'Dashboard',
      icon: 'dashboard',
      link: ['/']
    },
    {
      title: 'Manage users',
      icon: 'group',
      link: ['/users']
    },
    {
      class: ProjectsSidenavComponent,
      data: {}
    },
    {
      class: ServersSidenavComponent,
      data: {}
    }
  ];

  progressOptions = {
    min: 8,
    max: 100,
    ease: 'linear',
    speed: 200,
    trickleSpeed: 300,
    meteor: true,
    spinner: true,
    spinnerPosition: 'right',
    direction: 'ltr+',
    color: 'red',
    thick: false
  };

  constructor(
    public info: StaticService,
    private footerService: FooterService,
    private logoService: LogoService,
    private auth: AuthService,
    private userService: UserService,
    private menuService: MenuService,
    private router: Router,
    private echo: EchoService,
    private deployments: DeploymentsService,
    private projects: ProjectsService,
    private progress: NgProgress
  ) {
    this.info.getInfo();
    if (this.auth.token) {
      this.auth.getUser();
    }
    this.userService.logoutAction = () => this.auth.logout().then(res => this.router.navigate(['/login']));
  }

  ngOnInit() {
    // this.progress.start();

    this.footerService.setCurrent(this.footer);
    this.logoService.setCurrent(this.logo);
    this.info.info.subscribe((info: Info) => {
      this.ready = Boolean(info);
      if (info.siteName) {
        this.logo.html_lg = `<b>${info.siteName}</b>`;
        this.logoService.setCurrent(this.logo);
        // this.progress.complete();
      }
    });
    this.auth.user.subscribe(user => {
      const adminUser: AdminUser = new AdminUser({
        avatarUrl: user.avatar || 'http://www.puristaudiodesign.com/Data/images/misc/default-avatar.jpg',
        email: user.email,
        firstname: user.name,
        lastname: ''
      });
      this.userService.setCurrent(adminUser);
      this.registerEcho();
    });
    this.menuService.setCurrent(this.menu);
  }

  registerEcho() {
    const deployEvents = {
      '.deployment.created': (e: EchoEvent) => {
        console.log(Object.assign({}, e));
        this.deployments.newFromEcho(e.model)
      },
      '.deployment.updated': (e: EchoEvent) => {
        console.log(Object.assign({}, e));
        this.deployments.updateFromEcho(e.model)
      },
    };
    const projectEvents = {
      '.project.updated': (e: EchoEvent) => {
        console.log(Object.assign({}, e));
        this.projects.updateFromEcho(e.model)
      }
    };
    this.echo.registerChannel('deployment', deployEvents);
    this.echo.registerChannel('project', projectEvents);
  }
}
