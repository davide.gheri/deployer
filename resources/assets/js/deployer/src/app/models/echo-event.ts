
export interface EchoEvent {
  name: string;
  model: any;
  socket?: any;
}