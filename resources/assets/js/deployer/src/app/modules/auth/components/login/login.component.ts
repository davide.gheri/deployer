import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { StaticService } from '../../../core/services/static.service';
import { Credentials } from '../../models';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading: boolean;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router,
    public info: StaticService
  ) {
    !this.auth.expired() && this.onLogin()
  }

  get emailCtrl(): FormControl {
    return <FormControl>this.loginForm.get('email');
  }

  get passwordCtrl(): FormControl {
    return <FormControl>this.loginForm.get('password');
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', Validators.required]
    })
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.loading = true;
      this.auth.login(<Credentials>this.loginForm.value)
        .then(res => {
          this.loading = false;
          this.onLogin();
        })
        .catch(err => {
          this.loading = false;
          this.emailCtrl.setErrors({notFound: err.error.errors['email'][0] || 'Not found'});
        });
    }
  }

  onLogin() {
    this.router.navigate(['']);
  }

  getEmailErrorMessage() {
    return this.emailCtrl.hasError('required') ? 'Email required' :
      this.emailCtrl.hasError('email') ? 'Not a valid email' :
        this.emailCtrl.hasError('notFound') ? this.emailCtrl.getError('notFound') : 'Error'

  }
}
