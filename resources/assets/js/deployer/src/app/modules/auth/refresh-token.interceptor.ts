import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpHandler, HttpHeaderResponse, HttpInterceptor, HttpProgressEvent, HttpRequest, HttpResponse,
  HttpSentEvent, HttpUserEvent
} from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AuthService } from './services/auth.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/filter';
import 'rxjs/add/observable/throw';
import { Router } from '@angular/router';

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {
  isRefreshingToken: boolean = false;
  token$ = new BehaviorSubject<string>(null);

  constructor(
    private auth: AuthService,
    private router: Router,
  ) {}

  addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
    return req.clone({ setHeaders: { Authorization: 'Bearer ' + token }})
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
    return next.handle(req)
      .catch(error => {
        if (error instanceof  HttpErrorResponse) {
          if (error.status === 401) {
            if (error.error['error'] == 'invalid_request') {
              return this.handleInvalidRequest();
            } else {
              return this.handle401Error(req, next);
            }
          }
          return Observable.throw(error);
        } else {
          return Observable.throw(error);
        }
      })
  }

  handleInvalidRequest() {
    return Observable.fromPromise(this.auth.logout())
      .do(_ => this.router.navigate(['/login']));
  }

  handle401Error(req: HttpRequest<any>, next: HttpHandler) {
    if (!this.isRefreshingToken) {
      this.isRefreshingToken = true;
      this.token$.next(null);
      return this.auth.refreshToken()
        .switchMap((token: string) => {
          if (token) {
            this.token$.next(token);
            return next.handle(this.addToken(req, token));
          }
          return this.handleInvalidRequest();
        });
    } else {
      return this.token$
        .filter(token => token != null)
        .take(1)
        .switchMap(token => {
          return next.handle(this.addToken(req, token));
        });
    }
  }

}