import { Injectable } from '@angular/core';
import { ApiService } from '../../core/services/api.service';
import { environment } from '../../../../environments/environment';
import { Credentials, AuthResponse } from '../models';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/of';
import { Subject } from 'rxjs/Subject';
import { User } from '../../users/models/user';

@Injectable()
export class AuthService {

  readonly TOKEN = 'access_token';
  readonly REFRESH_TOKEN = 'refresh_token';

  jwt: JwtHelperService;
  // public user$: Observable<any>;
  private user$ = new Subject<User>();

  constructor(
    private api: ApiService,
  ) {
    this.jwt = new JwtHelperService();
  }

  login(data: Credentials): any {
    return this.api.post(environment.api.auth.login, data).toPromise()
      .then((res: AuthResponse) => {
        this.storeData(res);
        return res;
      })
  }

  logout(): any {
    return new Promise((res, rej) => {
      try {
        this.removeData();
        res();
      } catch (e) {
        rej(e)
      }
    })
  }

  refreshToken() {
    return this.refresh_token ? this.api.post(environment.api.auth.refresh, {token: this.refresh_token})
      .do((res: AuthResponse) => {
        if (res[this.TOKEN]) {
          this.storeData(res);
        }
      })
      .map(res => this.token) : Observable.of('');
  }

  getUser() {
    const get = this.api.get(environment.api.user);
    get.subscribe(user => this.user$.next(user));
    return get;
  }

  storeData(res: AuthResponse) {
    this.token = res[this.TOKEN];
    this.getUser();
    this.refresh_token = res[this.REFRESH_TOKEN];
  }

  removeData() {
    localStorage.removeItem(this.TOKEN);
    localStorage.removeItem(this.REFRESH_TOKEN);
  }

  expired(): any {
    return this.jwt.isTokenExpired(this.token);
  }

  get token(): string {
    return localStorage.getItem(this.TOKEN);
  }

  set token(token: string) {
    localStorage.setItem(this.TOKEN, token)
  }

  get refresh_token(): string {
    return localStorage.getItem(this.REFRESH_TOKEN);
  }

  set refresh_token(token: string) {
    localStorage.setItem(this.REFRESH_TOKEN, token);
  }

  get user(): Observable<User> {
    return this.user$.asObservable();
  }

}
