import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommandsFormListComponent } from './components/commands-form-list/commands-form-list.component';
import { CoreModule } from '../core/core.module';
import { CommandsService } from './services/commands.service';
import { CommandFormEditComponent } from './components/commands-form-list/command-form-edit/command-form-edit.component';
import { SortablejsModule } from 'angular-sortablejs';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    SortablejsModule
  ],
  exports: [
    CommandsFormListComponent
  ],
  declarations: [
    CommandsFormListComponent,
    CommandFormEditComponent
  ],
  providers: [CommandsService]
})
export class CommandsModule { }
