import { Component, Input, OnInit } from '@angular/core';
import { Command } from '../../../models/command';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommandsService } from '../../../services/commands.service';
import { Observable } from 'rxjs/Observable';
import { CustomValidators } from 'ng2-validation';

declare const $: any;

@Component({
  selector: 'app-command-form-edit',
  templateUrl: './command-form-edit.component.html',
  styleUrls: ['./command-form-edit.component.scss']
})
export class CommandFormEditComponent implements OnInit {
  private _command: Command;
  @Input() command: Observable<Command>;
  @Input() projectId: string;
  @Input() deployStepId: string;

  commandForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private commands: CommandsService
  ) { }

  ngOnInit() {
    this.commandForm = this.fb.group({
      name: ['', Validators.required],
      script: [''],
      weight: [999, CustomValidators.range([0, 10000])]
    });
    this.command.subscribe((command: Command) => {
      this.commandForm.patchValue(command);
      this._command = command;
    });
  }

  onSubmit() {
    if (this.commandForm.valid) {
      const data = Object.assign(this.commandForm.value, {custom: 1});
      this.commands.update(this._command.id, data, this.projectId, this.deployStepId)
        .then(res => {
          this.commandForm.reset({weight: 999});
          $('#command_edit').modal('toggle')
        })
        .catch(err => this.handleErrors(err))
    } else {
      Object.keys(this.commandForm.controls).forEach(name => {
        this.commandForm.get(name).markAsTouched()
      })
    }
  }

  private handleErrors(err) {
    if (err.status === 422) {
      const { errors } = err.error;
      Object.keys(errors).forEach(field => {
        if (this.commandForm.get(field)) {
          this.commandForm.get(field).setErrors({error: errors[field][0]})
        }
      });
    }
  }

}
