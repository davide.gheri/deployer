import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CommandsService } from '../../services/commands.service';
import { Command } from '../../models/command';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { BreadcrumbService } from 'ngx-admin-lte';
import { Subject } from 'rxjs/Subject';

declare const $: any;

@Component({
  selector: 'app-commands-form-list',
  templateUrl: './commands-form-list.component.html',
  styleUrls: ['./commands-form-list.component.scss']
})
export class CommandsFormListComponent implements OnInit, OnDestroy {
  commandForm: FormGroup;
  command$ = new Subject<Command>();
  projectId: number;
  deployStepId: number;

  sortOptions: any;

  @ViewChild('list') list: ElementRef;

  constructor(
    public commands: CommandsService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private bread: BreadcrumbService
  ) {
    this.sortOptions = {
      onUpdate: (e: any) => {
        const item: any = e.item;
        const next: any = item.nextElementSibling;
        let newWeight = 0;
        if (next) {
          newWeight = next.dataset['weight'] - 1;
        } else {
          const prev: any = item.previousElementSibling;
          newWeight = parseInt(prev.dataset['weight']) + 1;
        }
        let command: Command = {};
        try {
          command = JSON.parse(item.dataset['command']);
        } catch (e) {
          return false;
        }
        this.commands.update(command.id, {weight: newWeight}, this.projectId, this.deployStepId, false)
          .then(res => {

            console.log(res)
          })
      }
    }
  }

  ngOnInit() {
    this.route.params.subscribe(data => {
      const { id, deploy_step_id } = data;
      this.projectId = id;
      this.deployStepId = deploy_step_id;
      this.commands.getAll(id, deploy_step_id);
      this.bread.setCurrent({
        display: true,
        header: 'Commands',
        levels: [
          {
            icon: 'dashboard',
            link: ['/'],
          },
          {
            icon: 'th-list',
            link: ['/projects'],
            title: 'Projects'
          },
          {
            icon: 'git',
            link: ['/projects', id],
          },
          {
            icon: 'edit',
            link: [],
            title: 'Edit commands'
          }
        ]
      });
    });

    this.commandForm = this.fb.group({
      name: ['', Validators.required],
      script: [''],
      weight: [999, CustomValidators.range([0, 10000])]
    });

  }

  ngOnDestroy() {
    this.bread.clear();
  }

  getCommandType(command: Command, printClass: boolean = false) {
    const type = command.custom;
    switch (type) {
      case false:
        return printClass ? 'info' : 'Native';
      case true:
        return printClass ? 'warning' : 'Custom';
    }
  }

  deleteCommand(e, command: Command) {
    e.preventDefault();
    const c = confirm('Are you sure?');
    if (c) {
      this.commands.delete(command.id, this.projectId, this.deployStepId)
        .then(res => console.log(res));
    }
  }

  editCommand(command: Command) {
    if (command.custom) {
      this.command$.next(command);
      $('#command_edit').modal('toggle');
    }
  }

  onSubmit() {
    if (this.commandForm.valid) {
      const data = Object.assign(this.commandForm.value, {custom: 1});
      this.commands.create(data, this.projectId, this.deployStepId)
        .then(res => this.commandForm.reset({weight: 999}))
        .catch(err => this.handleErrors(err))
    } else {
      Object.keys(this.commandForm.controls).forEach(name => {
        this.commandForm.get(name).markAsTouched()
      })
    }
  }

  private handleErrors(err) {
    if (err.status === 422) {
      const { errors } = err.error;
      Object.keys(errors).forEach(field => {
        if (this.commandForm.get(field)) {
          this.commandForm.get(field).setErrors({error: errors[field][0]})
        }
      });
    }
  }

}
