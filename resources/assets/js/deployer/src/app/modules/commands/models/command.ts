
export interface Command {
  id?: string;
  deploy_step_id?: string;
  name?: string;
  script?: string;
  custom?: boolean;
  weight?: number;
}