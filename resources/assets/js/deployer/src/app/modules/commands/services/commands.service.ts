import { Injectable } from '@angular/core';
import { BaseService } from '../../core/services/base.service';
import { Command } from '../models/command';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CommandsService extends BaseService<Command> {
  private endPoint: string = 'commands';
  private modelName: string = 'Command';

  protected addNew(model: Command) {
    this.model$.next(model);
    this._models.push(model);
    this.sort();
  }

  protected updateModel(model: Command, id) {
    const oldU = this._models.findIndex(u => u['id'] === id);
    this._models = this._models.slice();
    this._models[oldU] = model;
    this.sort();
    this.models$.next(this._models);
  }

  private sort() {
    this._models.sort((a, b) => a.weight - b.weight);
  }

  get commands(): Observable<Command[]> {
    return this.models$.asObservable();
  }

  get command(): Observable<Command> {
    return this.model$.asObservable();
  }

}
