import { Injectable } from '@angular/core';
import {
  HttpHandler,
  HttpHeaderResponse,
  HttpInterceptor,
  HttpProgressEvent, HttpRequest,
  HttpResponse, HttpSentEvent,
  HttpUserEvent
} from '@angular/common/http';
import { EchoService } from './services/echo.service';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class AddSocketIdInterceptor implements HttpInterceptor {

  constructor(
    private echo: EchoService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): any {
    let newReq = req.clone();
    if (this.echo.echo && this.echo.echo.socketId()) {
      newReq = req.clone({setHeaders: {
          'X-Socket-ID': this.echo.echo.socketId()
      }});
    }
    return next.handle(newReq);
  }
}