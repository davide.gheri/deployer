import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.scss']
})
export class BoxComponent implements OnInit {
  @Input() layout?: string = 'box';
  @Input() title?: string;
  @Input() description?: string;
  @Input() header? = false;
  @Input() noPadding? = false;
  @Input() image?: string;
  @Input() loading?: boolean = false;
  @Input() collapsible: boolean = true;
  @Input() link?: string;
  private _type: string = 'default';
  @Input()
  set type(type: string) {
    this._type = type;
  }
  get type(): string {
    return this._type
  }
  private _collapsed: boolean = false;
  @Input()
  set collapsed(collapsed: boolean) {
    this._collapsed = collapsed && this.collapsible;
  }
  get collapsed(): boolean {
    return this._collapsed && this.collapsible;
  }

  constructor() { }

  ngOnInit() {
  }

}
