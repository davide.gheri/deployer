import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-form-group',
  templateUrl: './form-group.component.html',
  styleUrls: ['./form-group.component.scss']
})
export class FormGroupComponent implements OnInit {

  @Input() control: FormControl;
  @Input() label?: string;
  @Input() name?: string;

  constructor() { }

  ngOnInit() {
    if (!this.name) {
      this.name = this.getControlName();
    }
    if (!this.label) {
      this.label = this.name.charAt(0).toUpperCase() + this.name.slice(1)
    }
  }

  getControlName() {
    let controlName = Math.random() + '';
    Object.keys(this.control.parent.controls).forEach(name => {
      if (this.control === this.control.parent.controls[name]) {
        controlName = name;
      }
    });
    return controlName;
  }

}
