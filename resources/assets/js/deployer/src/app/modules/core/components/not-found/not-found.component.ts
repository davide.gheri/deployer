import { Component, OnDestroy, OnInit } from '@angular/core';
import { BreadcrumbService } from 'ngx-admin-lte';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit, OnDestroy {

  constructor(
    private bread: BreadcrumbService
  ) { }

  ngOnInit() {
    this.bread.setCurrent({
      display: true,
      header: '404',
      description: 'Not found',
    })
  }

  ngOnDestroy() {

  }

}
