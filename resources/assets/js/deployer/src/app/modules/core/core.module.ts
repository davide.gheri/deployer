import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiService } from './services/api.service';
import { StaticService } from './services/static.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from '../../app-routing.module';
import { AppAdminLteModule } from '../../app-admin-lte.module';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { HeaderComponent } from './components/header/header.component';
import { FirstPipe } from './pipes/first.pipe';
import { FormGroupComponent } from './components/form-group/form-group.component';
import { BoxComponent } from './components/box/box.component';
import { BaseService } from './services/base.service';
import { EchoService } from './services/echo.service';
import { AddSocketIdInterceptor } from './add-socket-id.interceptor';
import { LogPipe } from './pipes/log.pipe';
import { BoxFooterDirective } from './directives/box-footer.directive';
import { FormatPipe } from './pipes/format.pipe';
import { TranslateModule } from '@ngx-translate/core';
import { NotFoundComponent } from './components/not-found/not-found.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    AppAdminLteModule,
    TranslateModule,
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    AppAdminLteModule,
    SidenavComponent,
    HeaderComponent,
    FirstPipe,
    FormGroupComponent,
    BoxComponent,
    LogPipe,
    BoxFooterDirective,
    FormatPipe,
    TranslateModule,
  ],
  declarations: [
    SidenavComponent,
    HeaderComponent,
    FirstPipe,
    FormGroupComponent,
    BoxComponent,
    LogPipe,
    BoxFooterDirective,
    FormatPipe,
    NotFoundComponent,
  ],
  providers: [
    ApiService,
    StaticService,
    BaseService,
    EchoService,
    {provide: HTTP_INTERCEPTORS, useClass: AddSocketIdInterceptor, multi: true}
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class CoreModule { }
