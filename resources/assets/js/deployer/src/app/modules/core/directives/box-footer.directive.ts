import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[boxFooter]'
})
export class BoxFooterDirective implements OnInit {

  constructor(
    private elementRef: ElementRef,
  ) { }

  ngOnInit() {
    this.elementRef.nativeElement.classList.add('box-footer');
  }

}
