export interface Info {
  siteName?: string;
  frontendUrl?: string;
  sshKey?: string;
}
