import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'first'
})
export class FirstPipe implements PipeTransform {

  transform(value: Object): any {
    const keys = Object.keys(value);
    if (keys.length) {
      return value[keys[0]] === true ? keys[0] : value[keys[0]]
    }
  }

}
