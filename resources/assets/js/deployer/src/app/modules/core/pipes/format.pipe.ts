import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'format'
})
export class FormatPipe implements PipeTransform {

  transform(value: any, format: string = 'DD/MM/YYYY HH:mm:ss'): any {
    const date = moment(value);
    if (date.isValid()) {
      return date.format(format);
    }
    return value;
  }

}
