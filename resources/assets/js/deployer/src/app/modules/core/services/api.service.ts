import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Router } from '@angular/router';

@Injectable()
export class ApiService {

  headers: HttpHeaders;

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.headers = new HttpHeaders({});
  }

  addHeader(name, value) {
    this.headers.append(name, value);
  }

  getOptions() {
    return {headers: this.headers};
  }

  post<T>(url, data): Observable<T> {
    return this.http.post<T>(url, data, this.getOptions());
  }

  get(url): Observable<any> {
    return this.http.get(url, this.getOptions())
      .catch(err => {
        if (err.status === 404) {
          this.router.navigate(['404']);
        }
        return Observable.throw(err)
      })
  }

  patch<T>(url, data): Observable<T> {
    return this.http.patch<T>(url, data, this.getOptions());
  }

  destroy<T>(url): Observable<T> {
    return this.http.delete<T>(url, this.getOptions());
  }

}
