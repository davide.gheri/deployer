import { ToasterService } from 'angular2-toaster';
import { ApiService } from './api.service';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from '../../../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { NotificationsService } from 'ngx-admin-lte';

@Injectable()
export class BaseService<T> { //Should be Abstract but ng compiler breaks
  protected loading$ = new BehaviorSubject<boolean>(false);
  protected models$ = new BehaviorSubject<T[]>([]);
  public model$ = new Subject<T>();

  protected _models: T[] = [];

  constructor(
    protected api: ApiService,
    protected toastr: ToasterService,
    protected notification: NotificationsService,
  ) { }

  get end_point(): string {
    if (this['endPoint']) {
      return this['endPoint']
    } else {
      throw new Error("Service doesn't define the Api endPoint");
    }
  }

  get model_name(): string | null {
    return this['modelName'] || null
  }

  public getAll(parentId?, subParentId?) {
    this.loading$.next(true);
    this._models = [];
    this.models$.next([]);
    return this.api.get(this.resolveUrl('index', null, parentId, subParentId)).toPromise()
      .then((res: T[]) => {
        if (res) {
          this.loading$.next(false);
          this.models$.next(res);
          this._models = res;
        }
        return res;
      })
  }

  public get(id, parentId?, subParentId?) {
    const get = this.api.get(this.resolveUrl('show', id, parentId, subParentId));
    get.do(model => this.model$.next(model));
    return get;
  }

  public create(data, parentId?, subParentId?) {
    this.loading$.next(true);
    return this.api.post(this.resolveUrl('store', null, parentId, subParentId), data).toPromise()
      .then((res: T) => {
        this.loading$.next(false);
        this.addNew(res);
        this.toastr.pop('success', this.model_name + ' created');
        return res;
      })
  }

  public update(id, data, parentId?, subParentId?, load: boolean = true) {
    if (load) {
      this.loading$.next(true);
    }
    const patch = this.api.patch(this.resolveUrl('update', id, parentId, subParentId), data);
    return patch.toPromise()
      .then((model: T) => {
        this.loading$.next(false);
        this.updateModel(model, id);
        this.toastr.pop('success', this.model_name + ' updated');
        return model;
      })
  }

  public delete(id, parentId?, subParentId?) {
    this.loading$.next(true);
    const destroy = this.api.destroy(this.resolveUrl('destroy', id, parentId, subParentId));
    return destroy.toPromise()
      .then(model => {
        this.loading$.next(false);
        this._models = this._models.filter(u => u['id'] !== id);
        this.models$.next(this._models);
        this.toastr.pop('info', this.model_name + ' deleted');
        return model;
      });
  }

  protected resolveUrl(type, id?, parentId?, subParentId?): string {
    let url = environment.api[this.end_point][type];
    if (url) {
      if (id) {
        url = url.replace(':id', id);
      }
      if (parentId) {
        url = url.replace(':parent_id', parentId)
      }
      if (subParentId) {
        url = url.replace(':sub_parent_id', subParentId)
      }
    } else {
      throw new Error('Url not found');
    }
    return url;
  }

  protected addNew(model: T) {
    this.model$.next(model);
    this._models.push(model);
  }

  protected updateModel(model: T, id) {
    const oldU = this._models.findIndex(u => u['id'] === id);
    this._models = this._models.slice();
    this._models[oldU] = model;
    this.models$.next(this._models);
  }

  get loading(): Observable<boolean> {
    return this.loading$.asObservable();
  }

}