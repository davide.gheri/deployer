import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { AuthService } from '../../auth/services/auth.service';
import { ApiService } from './api.service';

declare var require: any;
declare var window: any;

//Workaround to use NodeJs modules in Typescript (needs @types/node)
const Pusher = require('pusher-js');
const Echo = require('laravel-echo');

@Injectable()
export class EchoService {
  public echo: any;

  constructor(
    private auth: AuthService,
    private api: ApiService
  ) {
    this.auth.user.subscribe(user => {
      if (user) {
        // if (window.io) {
          this.echo = new Echo({
            broadcaster: 'pusher',
            key: 'd4e75f65455b74f0f0e1',
            cluster: 'eu',
            encrypted: true,
            host: `http${environment.isHttps ? 's' : ''}://${environment.host}`,
            authEndpoint: `http${environment.isHttps ? 's' : ''}://${environment.host}/broadcasting/auth`,
            auth: {
              headers: {
                Authorization: 'Bearer ' + this.auth.token
              }
            }
          });
        // } else {
        //   alert('Socket.io not Found! Please check the server configuration')
        // }
      }
    })
  }

  registerChannel(channel, events: any) {
    if (this.echo) {
      const register = this.echo.private(channel);
      Object.keys(events).forEach((event) => {
        register.listen(event, (e) => events[event](e));
        console.log(`${event} registered`)
      });
    }
  }

}
