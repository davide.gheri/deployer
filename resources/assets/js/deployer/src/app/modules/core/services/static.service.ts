import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Info } from '../models/info';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class StaticService {

  private info$ = new BehaviorSubject<Info>({});

  constructor(
    private api: ApiService
  ) { }

  getInfo() {
    this.api.get(environment.api.info)
      .subscribe((res: Info) => this.info$.next(res));
  }

  get info(): Observable<Info> {
    return this.info$.asObservable();
  }

}
