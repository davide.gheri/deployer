import { Component, OnDestroy, OnInit } from '@angular/core';
import { BreadcrumbService } from 'ngx-admin-lte';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  constructor(
    private bread: BreadcrumbService
  ) { }

  ngOnInit() {
    this.bread.setCurrent({
      display: true,
      header: 'Dashboard',
    })
  }

  ngOnDestroy() {
    this.bread.clear();
  }

}
