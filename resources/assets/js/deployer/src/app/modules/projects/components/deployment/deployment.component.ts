import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BreadcrumbService } from 'ngx-admin-lte';
import { DeploymentsService } from '../../services/deployments.service';
import { Subscription } from 'rxjs/Subscription';
import { Deployment } from '../../models/deployment';
import { Project } from '../../models/project';

@Component({
  selector: 'app-deployment',
  templateUrl: './deployment.component.html',
  styleUrls: ['./deployment.component.scss']
})
export class DeploymentComponent implements OnInit, OnDestroy {
  _sub: Subscription;
  deployment: Deployment;
  project: Project;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private bread: BreadcrumbService,
    public deployments: DeploymentsService
  ) { }

  ngOnInit() {
    //TODO verify if snapshot is ok or subscribe
    const { id, deploy_id } = this.route.snapshot.params;
    this._sub = this.deployments.get(deploy_id, id).subscribe((deployment: Deployment) => {
      this.deployment = deployment;
      this.project = deployment.project;
      this.deployments.currentId = deployment.id;
      this.bread.setCurrent({
        display: true,
        header: deployment.id,
        description: deployment.project.name,
        levels: [
          {
            icon: 'dashboard',
            link: ['/'],
          },
          {
            icon: 'th-list',
            link: ['/projects'],
            title: 'Projects'
          },
          {
            icon: 'git',
            link: ['/projects', deployment.project.id],
            title: deployment.project.name
          },
          {
            icon: 'cloud-upload',
            link: [],
            title: 'Deploy'
          }
        ]
      });
    });
  }

  ngOnDestroy() {
    this.bread.clear();
    this._sub.unsubscribe();
  }

  shouldShowCommitInfo(): boolean {
    return Boolean(this.deployment) && Boolean(this.deployment.commit);
  }

  getStatus() {
    if (this.deployment) {
      const status = this.deployment.status;
      switch (status) {
        case 0:
          return 'success';
        case 1:
          return 'warning';
        case 2:
          return 'info';
        default:
          return 'danger';
      }
    }
    return 'default';
  }

}
