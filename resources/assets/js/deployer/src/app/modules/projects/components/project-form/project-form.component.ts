import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Project } from '../../models/project';
import { ActivatedRoute, Router } from '@angular/router';
import { BreadcrumbService } from 'ngx-admin-lte';
import { ProjectsService } from '../../services/projects.service';
import { CustomValidators } from 'ng2-validation';

@Component({
  selector: 'app-project-form',
  templateUrl: './project-form.component.html',
  styleUrls: ['./project-form.component.scss']
})
export class ProjectFormComponent implements OnInit, OnDestroy {
  isEdit: boolean;
  projectForm: FormGroup;
  public project: Project;

  constructor(
    private fb: FormBuilder,
    private projects: ProjectsService,
    private router: Router,
    private route: ActivatedRoute,
    private bread: BreadcrumbService,
  ) { }

  ngOnInit() {
    this.projectForm = this.fb.group({
      name: ['', Validators.required],
      repository: ['', Validators.required],
      branch: ['master', Validators.required],
      url: ['', CustomValidators.url],
    });

    this.route.data.subscribe(data => {
      if (data.project) {
        this.isEdit = true;
        this.project = data.project;
        this.projectForm.patchValue(data.project);
        this.bread.setCurrent({
          display: true,
          header: data.project.name,
          description: 'Edit project',
          levels: [
            {
              icon: 'dashboard',
              link: ['/'],
            },
            {
              icon: 'th-list',
              link: ['/projects'],
              title: 'Projects'
            },
            {
              icon: 'git',
              link: ['/projects', data.project.id],
              title: data.project.name
            },
            {
              icon: 'edit',
              link: [],
              title: 'Edit'
            }
          ]
        });
      } else {
        this.isEdit = false;
        this.bread.setCurrent({
          display: true,
          header: 'New project',
          levels: [
            {
              icon: 'dashboard',
              link: ['/'],
            },
            {
              icon: 'th-list',
              link: ['/projects'],
              title: 'Projects'
            },
            {
              icon: 'git',
              link: [],
              title: 'New'
            }
          ]
        });
      }
    });
  }

  ngOnDestroy() {
    this.bread.clear();
  }

  onSubmit() {
    if (this.projectForm.valid) {
      if (this.isEdit) {
        this.updateProject();
      } else {
        this.createProject();
      }
    } else {
      Object.keys(this.projectForm.controls).forEach(name => {
        this.projectForm.get(name).markAsTouched()
      })
    }
  }

  private createProject() {
    const data: Project = this.projectForm.value;
    if (data['url'] === '' || !data['url']) {
      delete data['url'];
    }
    this.projects.create(data)
      .then(res => {
        this.router.navigate(['projects', res.id]);
      })
      .catch(err => this.handleErrors(err))
  }

  private updateProject() {
    const data: Project = this.projectForm.value;
    if (data['url'] === '' || !data['url']) {
      delete data['url'];
    }
    this.projects.update(this.project.id, data)
      .then(res => {
        this.router.navigate(['projects', res.id]);
      })
      .catch(err => this.handleErrors(err))
  }

  onDelete() {
    const c = confirm('Are you sure?');
    if (c) {
      this.projects.delete(this.project.id)
        .then(res => this.router.navigate(['/projects']));
    }
  }

  private handleErrors(err) {
    if (err.status === 422) {
      const { errors } = err.error;
      Object.keys(errors).forEach(field => {
        if (this.projectForm.get(field)) {
          this.projectForm.get(field).setErrors({error: errors[field][0]})
        }
      });
    }
  }

  get nameCtrl(): FormControl {
    return this.projectForm.get('name') as FormControl;
  }

  get repoCtrl(): FormControl {
    return this.projectForm.get('repository') as FormControl;
  }

  get branchCtrl(): FormControl {
    return this.projectForm.get('branch') as FormControl;
  }

  get urlCtrl(): FormControl {
    return this.projectForm.get('url') as FormControl;
  }

}
