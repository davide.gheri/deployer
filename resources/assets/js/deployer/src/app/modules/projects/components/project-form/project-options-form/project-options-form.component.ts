import { Component, Input, OnInit } from '@angular/core';
import { Project } from '../../../models/project';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProjectsService } from '../../../services/projects.service';

@Component({
  selector: 'app-project-options-form',
  templateUrl: './project-options-form.component.html',
  styleUrls: ['./project-options-form.component.scss']
})
export class ProjectOptionsFormComponent implements OnInit {
  @Input() project: Project;
  optionsForm: FormGroup;
  files: any = [];
  shareds: any = [];

  constructor(
    private fb: FormBuilder,
    private projects: ProjectsService
  ) { }

  ngOnInit() {
    this.optionsForm = this.fb.group({
      files: this.fb.array([]),
      shareds: this.fb.array([]),
    });
    if (this.project.options) {
      if (this.project.options.hasOwnProperty('files')) {
        this.project.options.files.forEach((file, i) => {
          // if (i === 0) {
          //   this.formFiles.patchValue([file]);
          // } else {
            this.formFiles.push(this.createFile(file.origin, file.destination))
          // }
        })
      }
      if (this.project.options.hasOwnProperty('shareds')) {
        this.project.options.shareds.forEach((shared, i) => {
          // if (i === 0) {
          //   this.formShareds.patchValue([shared]);
          // } else {
            this.formShareds.push(this.createShared(shared.path));
          // }
        })
      }
    }
  }

  createFile(origin: string = '', destination: string = ''): FormGroup {
    return this.fb.group({
      origin: [origin, Validators.required],
      destination: [destination, Validators.required]
    });
  }

  createShared(path: string = ''): FormGroup {
    return this.fb.group({
      path: [path, Validators.required]
    });
  }

  addFile() {
    this.files = this.formFiles;
    this.files.push(this.createFile());
  }

  addShared() {
    this.shareds = this.formShareds;
    this.shareds.push(this.createShared());
  }

  deleteFile(i) {
    this.formFiles.removeAt(i);
  }

  deleteShared(i) {
    this.formShareds.removeAt(i);
  }

  onSubmit() {
    if (this.optionsForm.valid) {
      this.projects.updateOptions(this.project.id, this.optionsForm.value)
        .then(res => console.log(res))
        .catch(res => console.log(res));
    } else {
      this.markAsTouched(this.optionsForm);
    }
  }

  private markAsTouched(formGroup) {
    Object.keys(formGroup.controls).forEach(name => {
      const elem = formGroup.get(name);
      if (elem instanceof FormArray || elem instanceof FormGroup) {
        this.markAsTouched(elem);
      } else {
        elem.markAsTouched();
      }
    })
  }

  get formFiles(): FormArray {
    return this.optionsForm.get('files') as FormArray;
  }

  get formShareds(): FormArray {
    return this.optionsForm.get('shareds') as FormArray;
  }

}
