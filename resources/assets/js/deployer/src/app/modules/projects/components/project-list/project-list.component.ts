import { Component, OnDestroy, OnInit } from '@angular/core';
import { ProjectsService } from '../../services/projects.service';
import { Project } from '../../models/project';
import { Router } from '@angular/router';
import { BreadcrumbService } from 'ngx-admin-lte';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit, OnDestroy {

  constructor(
    public projects: ProjectsService,
    private router: Router,
    private bread: BreadcrumbService,
  ) { }

  ngOnInit() {
    this.bread.setCurrent({
      display: true,
      header: 'Projects',
      levels: [
        {
          icon: 'dashboard',
          link: ['/'],
        },
        {
          icon: 'th-list',
          link: ['/projects'],
          title: 'Projects'
        }
      ]
    });
  }

  ngOnDestroy() {
    this.bread.clear();
  }

  deleteProject(e, project: Project) {
    e.preventDefault();
    const c = confirm('Are you sure?');
    if (c) {
      this.projects.delete(project.id)
        .then(res => this.router.navigate(['/projects']));
    }
  }

}
