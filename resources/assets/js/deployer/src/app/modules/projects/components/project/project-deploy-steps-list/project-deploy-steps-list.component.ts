import { Component, Input, OnInit } from '@angular/core';
import { DeployStepsService } from '../../../services/deploy-steps.service';
import { Project } from '../../../models/project';

@Component({
  selector: 'app-project-deploy-steps-list',
  templateUrl: './project-deploy-steps-list.component.html',
  styleUrls: ['./project-deploy-steps-list.component.scss']
})
export class ProjectDeployStepsListComponent implements OnInit {
  private _project: Project;
  @Input()
  set project(value: Project) {
    if (this._project && this._project.id !== value.id) {
      this.deploySteps.getAll(value.id);
    }
    this._project = value;
  }

  get project(): Project {
    return this._project;
  }

  constructor(
    public deploySteps: DeployStepsService
  ) { }

  ngOnInit() {
    this.deploySteps.getAll(this.project.id);
  }

}
