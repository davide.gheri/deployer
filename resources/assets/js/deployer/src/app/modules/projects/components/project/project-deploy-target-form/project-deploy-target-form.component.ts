import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Project } from '../../../models/project';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DeployTargetsService } from '../../../services/deploy-targets.service';
import { ServersService } from '../../../../servers/services/servers.service';
import { DeployTarget } from '../../../models/deploy-target';
import { filter } from 'rxjs/operators';

declare var $: any;

@Component({
  selector: 'app-project-deploy-target-form',
  templateUrl: './project-deploy-target-form.component.html',
  styleUrls: ['./project-deploy-target-form.component.scss']
})
export class ProjectDeployTargetFormComponent implements OnInit, OnDestroy {
  @Input() project: Project;
  @Input() deployTarget?: DeployTarget;

  @Output() close = new EventEmitter();
  deployTargetForm: FormGroup;
  isEdit: boolean = false;

  constructor(
    private fb: FormBuilder,
    private deployTargets: DeployTargetsService,
    public servers: ServersService
  ) { }

  ngOnInit() {
    this.deployTargetForm = this.fb.group({
      server_id: ['', Validators.required],
      path: ['', Validators.required],
      user: [''],
      automatic: false,
      active: true,
    });

    this.serverCtrl.valueChanges.switchMap(val => {
      return this.servers.servers.map(s => {
        return s.find(server => server.id == val);
      })
    }).subscribe(s => {
      if (s) {
        this.userCtrl.setValue(s.user)
      }
    });

    this.isEdit = !!this.deployTarget;
    if (this.deployTarget) {
      this.deployTargetForm.patchValue(this.deployTarget);
    }
  }

  ngOnDestroy() {
  }

  onSubmit() {
    if (this.deployTargetForm.valid) {
      if (this.isEdit) {
        this.updateDeployTarget();
      } else {
        this.createDeployTarget();
      }
    }
  }

  createDeployTarget() {
    this.deployTargets.create(this.deployTargetForm.value, this.project.id)
      .then(res => {
        this.closeModal();
      })
  }

  updateDeployTarget() {
    this.deployTargets.update(this.deployTarget.id, this.deployTargetForm.value, this.project.id)
      .then(res => {
        this.closeModal();
      })
  }

  onDelete() {
    const c = confirm('Are you sure?');
    if (c) {
      this.deployTargets.delete(this.deployTarget.id, this.project.id)
        .then(res => {
          this.closeModal();
        })
    }
  }

  closeModal() {
    $('.modal').modal('hide');
    this.close.emit();
  }

  get serverCtrl(): FormControl {
    return this.deployTargetForm.get('server_id') as FormControl;
  }

  get userCtrl(): FormControl {
    return this.deployTargetForm.get('user') as FormControl;
  }

}
