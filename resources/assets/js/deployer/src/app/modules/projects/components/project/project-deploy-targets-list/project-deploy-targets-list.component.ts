import { Component, Input, OnInit } from '@angular/core';
import { Project } from '../../../models/project';
import { DeployTargetsService } from '../../../services/deploy-targets.service';

declare var $: any;

@Component({
  selector: 'app-project-deploy-targets-list',
  templateUrl: './project-deploy-targets-list.component.html',
  styleUrls: ['./project-deploy-targets-list.component.scss']
})
export class ProjectDeployTargetsListComponent implements OnInit {
  @Input() project: Project;

  constructor(
    public deployTargets: DeployTargetsService,
  ) { }

  ngOnInit() {
    this.deployTargets.getAll(this.project.id)
  }
}
