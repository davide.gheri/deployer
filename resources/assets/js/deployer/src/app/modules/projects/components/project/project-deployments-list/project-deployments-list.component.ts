import { Component, Input, OnInit } from '@angular/core';
import { Project } from '../../../models/project';
import { DeploymentsService } from '../../../services/deployments.service';

@Component({
  selector: 'app-project-deployments-list',
  templateUrl: './project-deployments-list.component.html',
  styleUrls: ['./project-deployments-list.component.scss']
})
export class ProjectDeploymentsListComponent implements OnInit {
  private _project: Project;
  @Input()
  set project(value: Project) {
    if (this._project && this._project.id !== value.id) {
      this.deployments.getAll(value.id);
    }
    this._project = value;
  }

  get project(): Project {
    return this._project;
  }

  constructor(
    public deployments: DeploymentsService
  ) { }

  ngOnInit() {
    this.deployments.getAll(this.project.id);
  }

}
