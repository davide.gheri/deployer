import { Component, Input, OnInit } from '@angular/core';
import { Project } from '../../../models/project';
import { ProjectsService } from '../../../services/projects.service';

@Component({
  selector: 'app-project-success-rate',
  templateUrl: './project-success-rate.component.html',
  styleUrls: ['./project-success-rate.component.scss']
})
export class ProjectSuccessRateComponent implements OnInit {
  private _project: Project;
  @Input()
  set project(project: Project) {
    this._project = project;
  }

  get project(): Project {
    return this._project;
  }

  constructor(
    public projects: ProjectsService
  ) {
    this.projects.model$.subscribe(p => {
      console.log(p);
      this.project = p
    })
  }

  ngOnInit() {
  }

  successRate(project): string {
    if (project.success_rate) {
      return project.success_rate + ' <sup style=font-size:20px;>%</sup>'
    }
    return 'No deployments yet';
  }

  type(project): string {
    switch (true) {
      case project.success_rate === null:
        return 'default';
      case project.success_rate < 80:
        return 'danger';
      case project.success_rate < 100:
        return 'warning';
      case project.success_rate === 100:
        return 'success';
      default:
        return 'default'
    }
  }

}
