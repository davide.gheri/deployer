import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BreadcrumbService } from 'ngx-admin-lte';
import { ProjectsService } from '../../services/projects.service';
import { Project } from '../../models/project';
import { DeploymentsService } from '../../services/deployments.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit, OnDestroy {
  project: Project;
  deploying: boolean = false;
  toDeploy: boolean = false;

  _sub: Subscription;

  constructor(
    private projects: ProjectsService,
    private router: Router,
    private route: ActivatedRoute,
    private bread: BreadcrumbService,
    private deployments: DeploymentsService
  ) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.project = data.project;
      this.projects.model$.next(data.project);
      this.projects.currentId = data.project.id;
      this.bread.setCurrent({
        display: true,
        header: data.project.name,
        description: data.project.id,
        levels: [
          {
            icon: 'dashboard',
            link: ['/'],
          },
          {
            icon: 'th-list',
            link: ['/projects'],
            title: 'Projects'
          },
          {
            icon: 'git',
            link: [],
            title: data.project.name
          }
        ]
      });
    });
    this._sub = this.projects.projects.subscribe((projects: Project[]) => {
      const currentProject = projects.find(p => p.id === this.project.id);
      if (currentProject) {
        this.deploying = currentProject.status == 1 || currentProject.status == 2;
      }
    });
  }

  ngOnDestroy() {
    this.bread.clear();
    this._sub.unsubscribe();
  }

  deleteProject(e) {
    e.preventDefault();
    const c = confirm('Are you sure?');
    if (c) {
      this.projects.delete(this.project.id)
        .then(res => this.router.navigate(['projects']));
    }
  }

  deploy() {
    this.toDeploy = true;
    this.deployments.create([], this.project.id)
      .then(res => this.toDeploy = false);
  }

}
