import { Component, OnChanges, OnInit } from '@angular/core';
import { ProjectsService } from '../../services/projects.service';

@Component({
  selector: 'app-projects-sidenav',
  templateUrl: './projects-sidenav.component.html',
  styleUrls: ['./projects-sidenav.component.scss']
})
export class ProjectsSidenavComponent implements OnInit, OnChanges {

  constructor(
    public projects: ProjectsService
  ) { }

  ngOnInit() {
    this.projects.getAll();
  }

  ngOnChanges() {
  }

}
