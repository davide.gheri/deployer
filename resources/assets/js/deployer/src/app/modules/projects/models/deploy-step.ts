
export interface DeployStep {
  id?: string;
  name?: string;
  description?: string;
  step?: number;
  commands?: any;
}