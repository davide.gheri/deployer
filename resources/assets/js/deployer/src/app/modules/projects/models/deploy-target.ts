import { Project } from './project';
import { Server } from '../../servers/models/server';

export interface DeployTarget {
  id?: string;
  path?: string;
  user?: string;
  automatic?: boolean;
  active?: boolean;
  project?: Project;
  server?: Server;
}