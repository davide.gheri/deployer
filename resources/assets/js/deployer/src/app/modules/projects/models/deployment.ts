import { Project } from './project';

export interface Deployment {
  id?: string;
  project_id?: string;
  committer?: string;
  commit?: string;
  commit_message?: string;
  status?: string | number;
  started_at?: any;
  finished_at?: any;
  progress?: string;
  project?: Project;
}