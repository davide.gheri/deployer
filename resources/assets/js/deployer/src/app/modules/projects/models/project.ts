
export interface Project {
  id?: string;
  name?: string;
  repository?: string;
  branch?: string;
  url?: string;
  last_run?: string;
  repository_path?: string;
  repository_url?: string;
  vcs_url?: string;
  status: number;
  avatar?: string;
  success_rate?: number;
  options?: {
    files?: {
      origin: string;
      destination: string
    }[],
    shareds?: {
      path: string;
    }[],
  };
}