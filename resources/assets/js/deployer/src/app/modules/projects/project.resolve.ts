import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Project } from './models/project';
import { Injectable } from '@angular/core';
import { RouterStateSnapshot } from '@angular/router/src/router_state';
import { Observable } from 'rxjs/Observable';
import { ProjectsService } from './services/projects.service';

@Injectable()
export class ProjectResolve implements Resolve<Project> {
  constructor(
    private projects: ProjectsService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Project> {
    const id = route.paramMap.get('id');
    
    return this.projects.get(id);
  }
}
