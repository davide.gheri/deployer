import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsService } from './services/projects.service';
import { CoreModule } from '../core/core.module';
import { ProjectsSidenavComponent } from './components/projects-sidenav/projects-sidenav.component';
import { ProjectFormComponent } from './components/project-form/project-form.component';
import { ProjectListComponent } from './components/project-list/project-list.component';
import { ProjectResolve } from './project.resolve';
import { ProjectComponent } from './components/project/project.component';
import { ProjectDetailsComponent } from './components/project/project-details/project-details.component';
import { ProjectDeploymentsListComponent } from './components/project/project-deployments-list/project-deployments-list.component';
import { DeploymentsService } from './services/deployments.service';
import { DeployTargetsService } from './services/deploy-targets.service';
import { ProjectDeployTargetsListComponent } from './components/project/project-deploy-targets-list/project-deploy-targets-list.component';
import { ProjectDeployTargetFormComponent } from './components/project/project-deploy-target-form/project-deploy-target-form.component';
import { DeploymentComponent } from './components/deployment/deployment.component';
import { ProjectSuccessRateComponent } from './components/project/project-success-rate/project-success-rate.component';
import { DeployStepsService } from './services/deploy-steps.service';
import { ProjectDeployStepsListComponent } from './components/project/project-deploy-steps-list/project-deploy-steps-list.component';
import { ProjectOptionsFormComponent } from './components/project-form/project-options-form/project-options-form.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
  ],
  exports: [
    ProjectsSidenavComponent
  ],
  declarations: [
    ProjectsSidenavComponent,
    ProjectFormComponent,
    ProjectListComponent,
    ProjectComponent,
    ProjectDetailsComponent,
    ProjectDeploymentsListComponent,
    ProjectDeployTargetsListComponent,
    ProjectDeployTargetFormComponent,
    DeploymentComponent,
    ProjectSuccessRateComponent,
    ProjectDeployStepsListComponent,
    ProjectOptionsFormComponent
  ],
  entryComponents: [ProjectsSidenavComponent, ProjectDeployTargetFormComponent],
  providers: [
    ProjectsService,
    ProjectResolve,
    DeploymentsService,
    DeployTargetsService,
    DeployStepsService,
  ]
})
export class ProjectsModule { }
