import { Injectable } from '@angular/core';
import { BaseService } from '../../core/services/base.service';
import { Observable } from 'rxjs/Observable';
import { DeployStep } from '../models/deploy-step';

@Injectable()
export class DeployStepsService extends BaseService<DeployStep> {
  private endPoint: string = 'deploySteps';
  private modelName: string = 'Deploy step';

  // getAll(project_id, page = 1) {
  //   this.loading$.next(true);
  //   return this.api.get(this.resolveUrl('index', null, project_id)).toPromise()
  //     .then((res: any) => {
  //       if (res.data) {
  //         this.loading$.next(false);
  //         this.models$.next(res.data);
  //         this._models = res.data;
  //       }
  //       return res.data;
  //     })
  // }

  get deploySteps(): Observable<DeployStep[]> {
    return this.models$.asObservable();
  }

  get deployStep(): Observable<DeployStep> {
    return this.model$.asObservable();
  }

}
