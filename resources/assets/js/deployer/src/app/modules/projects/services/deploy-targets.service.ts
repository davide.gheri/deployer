import { Injectable } from '@angular/core';
import { BaseService } from '../../core/services/base.service';
import { DeployTarget } from '../models/deploy-target';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DeployTargetsService extends BaseService<DeployTarget> {
  private endPoint: string = 'deployTargets';
  private modelName: string = 'Deploy target';

  get deployTargets(): Observable<DeployTarget[]> {
    return this.models$.asObservable();
  }

  get deployTarget(): Observable<DeployTarget> {
    return this.model$.asObservable();
  }

}
