import { Injectable } from '@angular/core';
import { Deployment } from '../models/deployment';
import { Observable } from 'rxjs/Observable';
import { BaseService } from '../../core/services/base.service';
import { Notification } from 'ngx-admin-lte';

@Injectable()
export class DeploymentsService extends BaseService<Deployment> {
  private endPoint: string = 'deployments';
  private modelName: string = 'Deployment';

  currentId: string;

  getAll(project_id, page = 1) {
    this.loading$.next(true);
    return this.api.get(this.resolveUrl('index', null, project_id)).toPromise()
      .then((res: any) => {
        if (res.data) {
          this.loading$.next(false);
          this.models$.next(res.data);
          this._models = res.data;
        }
        return res.data;
      })
  }

  get(id, parentId?) {
    this.api.get(this.resolveUrl('show', id, parentId)).toPromise()
      .then(res => this.model$.next(res));
    return this.deployment;
  }

  create(data, parentId?) {
    this.loading$.next(true);
    return this.api.post(this.resolveUrl('store', null, parentId), data).toPromise()
      .then((res: Deployment) => {
        this.loading$.next(false);
        this.addFromStart(res);
        this.toastr.pop('success', this.model_name + ' created');
        return res;
      })
  }

  newFromEcho(deployment: Deployment) {
    if (deployment.project_id != this._models[0].project_id) {
      return;
    }
    const old = this._models.find(d => d.id == deployment.id);
    console.log(deployment.id, this._models);
    if (!old) {
      this.addFromStart(deployment);
    }
  }

  updateFromEcho(deployment: Deployment) {
    this.notify(deployment);
    if (deployment.project_id != this._models[0].project_id) {
      return;
    }
    const oldIndex = this._models.findIndex(d => d.id === deployment.id);
    if (oldIndex >= 0) {
      const newModels = this._models.slice();
      newModels[oldIndex] = deployment;
      this._models = newModels;
      this.models$.next(newModels);
    } else {
      this.newFromEcho(deployment);
    }
    if (this.currentId && this.currentId === deployment.id) {
      // We should be in this deployment detail page, update the model$
      this.model$.next(deployment);
    }
  }

  addFromStart(deployment) {
    let newModels = this._models.slice();
    const oldIndex = this._models.findIndex(d => d.id === deployment.id);
    if (oldIndex < 0) {
      newModels.unshift(deployment);
    }
    if (newModels.length > 15) {
      newModels = newModels.slice(0, 15);
    }
    this._models = newModels;
    this.models$.next(newModels);
  }

  get deployments(): Observable<Deployment[]> {
    return this.models$.asObservable();
  }

  get deployment(): Observable<Deployment> {
    return this.model$.asObservable();
  }

  notify(deployment: Deployment) {
    let message: string, icon: string, toNotify: boolean;
    switch (deployment.status) {
      case 0:
        toNotify = true;
        message = 'Deploy completed';
        icon = 'check';
        break;
      case 3:
      case 4:
      case 6:
        toNotify = true;
        message = 'Deploy failed';
        icon = 'exclamation-triangle';
        break;
      default:
        toNotify = false;

    }
    if (toNotify) {
      this.notification.addNotification(new Notification({
        content: message + ' for project ' + deployment.project.name,
        class: 'fa fa-' + icon,
        link: '/projects/' + deployment.project_id
      }));
    }
  }

  getLabelStatus(deployment: Deployment, printClass: boolean = false) {
    const status = deployment.status;
    switch (status) {
      case 0:
        return printClass ? 'success' : 'Completed <i class="fa fa-check"></i>';
      case 1:
        return printClass ? 'warning' : 'Pending <i class="fa fa-clock-o"></i>';
      case 2:
        return printClass ? 'info' : 'Processing <i class="fa fa-cog fa-spin"></i>';
      default:
        return printClass ? 'danger' : 'Error <i class="fa fa-exclamation-triangle"></i>';
    }
  }

}
