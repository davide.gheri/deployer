import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Project } from '../models/project';
import { BaseService } from '../../core/services/base.service';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ProjectsService extends BaseService<Project> {
  private endPoint: string = 'projects';
  private modelName: string = 'Project';

  currentId: string;

  get projects(): Observable<Project[]> {
    return this.models$.asObservable();
  }

  get project(): Observable<Project> {
    return this.model$.asObservable();
  }

  updateOptions(id, data) {
    return this.api.patch(this.resolveUrl('updateOptions', id), {options: data}).toPromise()
      .then((model: Project) => {
        this.updateModel(model, id);
        this.toastr.pop('success', this.model_name + ' options updated');
        return model;
      })
  }

  updateFromEcho(project: Project) {
    const oldIndex = this._models.findIndex(p => p.id === project.id);
    if (oldIndex >= 0) {
      const newModels = this._models.slice();
      newModels[oldIndex] = project;
      this._models = newModels;
      this.models$.next(newModels);
    }
    if (this.currentId && this.currentId === project.id) {
      // We should be in this deployment detail page, update the model$
      this.model$.next(project);
    }
  }

}
