import { Component, OnDestroy, OnInit } from '@angular/core';
import { ServersService } from '../../services/servers.service';
import { Server } from '../../models/server';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BreadcrumbService } from 'ngx-admin-lte';
import { StaticService } from '../../../core/services/static.service';

@Component({
  selector: 'app-server-form',
  templateUrl: './server-form.component.html',
  styleUrls: ['./server-form.component.scss']
})
export class ServerFormComponent implements OnInit, OnDestroy {
  serverForm: FormGroup;
  server: Server;
  isEdit = false;

  constructor(
    public servers: ServersService,
    public info: StaticService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private bread: BreadcrumbService,
  ) { }

  ngOnInit() {
    this.serverForm = this.fb.group({
      name: ['', Validators.required],
      ip_address: ['', Validators.required],
      port: [22, Validators.required],
      user: ['', Validators.required],
    });

    this.route.data.subscribe(data => {
      if (data.server) {
        this.isEdit = true;
        this.server = data.server;
        this.serverForm.patchValue(data.server);
        this.bread.setCurrent({
          display: true,
          header: data.server.name,
          description: 'Edit server',
          levels: [
            {
              icon: 'dashboard',
              link: ['/'],
            },
            {
              icon: 'server',
              link: ['/servers'],
              title: 'Servers'
            },
            {
              icon: 'server',
              link: [],
              title: data.server.name
            }
          ]
        });
      } else {
        this.isEdit = false;
        this.bread.setCurrent({
          display: true,
          header: 'New server',
          levels: [
            {
              icon: 'dashboard',
              link: ['/'],
            },
            {
              icon: 'servers',
              link: ['/servers'],
              title: 'Servers'
            },
            {
              icon: 'server',
              link: [],
              title: 'New'
            }
          ]
        });
      }
    });
  }

  ngOnDestroy() {
    this.bread.clear();
  }

  onSubmit() {
    if (this.serverForm.valid) {
      if (this.isEdit) {
        this.updateServer();
      } else {
        this.createServer();
      }
    } else {
      Object.keys(this.serverForm.controls).forEach(name => {
        this.serverForm.get(name).markAsTouched()
      })
    }
  }

  private createServer() {
    const data: Server = this.serverForm.value;
    if (data['url'] === '' || !data['url']) {
      delete data['url'];
    }
    this.servers.create(data)
      .then(res => {
        this.router.navigate(['servers', res.id]);
      })
      .catch(err => this.handleErrors(err))
  }

  private updateServer() {
    const data: Server = this.serverForm.value;
    this.servers.update(this.server.id, data)
      .then(res => {
        this.router.navigate(['servers', res.id]);
      })
      .catch(err => this.handleErrors(err))
  }

  onDelete() {
    const c = confirm('Are you sure?');
    if (c) {
      this.servers.delete(this.server.id)
        .then(res => this.router.navigate(['/servers']));
    }
  }

  private handleErrors(err) {
    if (err.status === 422) {
      const { errors } = err.error;
      Object.keys(errors).forEach(field => {
        if (this.serverForm.get(field)) {
          this.serverForm.get(field).setErrors({error: errors[field][0]})
        }
      });
    }
  }

  getStatus(isClass = false) {
    switch (this.server.status) {
      case '0':
        return isClass ? 'success' : 'Success';
      case '1':
        return isClass ? 'warning' : 'Not tested';
      case '2':
        return isClass ? 'danger' : 'Failed';
      case '3':
        return isClass ? 'info' : 'Testing';
    }
  }

  testServer(e) {
    e.preventDefault();
    this.server.status = '3';
    this.servers.testConnection(this.server.id)
      .then(res => {
        this.servers.update(this.server.id, {status: res.status});
        this.server.status = res.status;
      })
      .catch(res => {
        this.servers.update(this.server.id, {status: '2'});
        this.server.status = '2';
      })
  }

  get nameCtrl(): FormControl {
    return this.serverForm.get('name') as FormControl;
  }

  get ipCtrl(): FormControl {
    return this.serverForm.get('ip_address') as FormControl;
  }

  get portCtrl(): FormControl {
    return this.serverForm.get('port') as FormControl;
  }

  get userCtrl(): FormControl {
    return this.serverForm.get('user') as FormControl;
  }

}
