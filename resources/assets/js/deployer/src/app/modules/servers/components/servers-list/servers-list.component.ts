import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BreadcrumbService } from 'ngx-admin-lte';
import { ServersService } from '../../services/servers.service';
import { Server } from '../../models/server';

@Component({
  selector: 'app-servers-list',
  templateUrl: './servers-list.component.html',
  styleUrls: ['./servers-list.component.scss']
})
export class ServersListComponent implements OnInit, OnDestroy {

  constructor(
    public servers: ServersService,
    private router: Router,
    private bread: BreadcrumbService,
  ) { }

  ngOnInit() {
    this.bread.setCurrent({
      display: true,
      header: 'Servers',
      levels: [
        {
          icon: 'dashboard',
          link: ['/'],
        },
        {
          icon: 'server',
          link: ['/servers'],
          title: 'Servers'
        }
      ]
    });
  }

  ngOnDestroy() {
    this.bread.clear();
  }

  deleteServer(e, server: Server) {
    e.preventDefault();
    const c = confirm('Are you sure?');
    if (c) {
      this.servers.delete(server.id)
        .then(res => this.router.navigate(['/servers']));
    }
  }

}
