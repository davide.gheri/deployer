import { Component, OnChanges, OnInit } from '@angular/core';
import { ServersService } from '../../services/servers.service';

@Component({
  selector: 'app-servers-sidenav',
  templateUrl: './servers-sidenav.component.html',
  styleUrls: ['./servers-sidenav.component.scss']
})
export class ServersSidenavComponent implements OnInit, OnChanges {

  constructor(
    public servers: ServersService
  ) { }

  ngOnInit() {
    this.servers.getAll();
  }

  ngOnChanges() {
  }

}
