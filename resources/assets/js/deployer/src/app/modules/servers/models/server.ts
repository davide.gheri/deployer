
export interface Server {
  id?: string;
  name?: string;
  ip_address?: string;
  user?: string;
  status?: string;
  port?: number;
}