import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { RouterStateSnapshot } from '@angular/router/src/router_state';
import { Observable } from 'rxjs/Observable';
import { Server } from './models/server';
import { ServersService } from './services/servers.service';

@Injectable()
export class ServerResolve implements Resolve<Server> {
  constructor(
    private servers: ServersService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Server> {
    const id = route.paramMap.get('id');
    return this.servers.get(id);
  }
}
