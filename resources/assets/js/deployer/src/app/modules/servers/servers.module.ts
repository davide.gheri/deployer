import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServersListComponent } from './components/servers-list/servers-list.component';
import { ServersSidenavComponent } from './components/servers-sidenav/servers-sidenav.component';
import { CoreModule } from '../core/core.module';
import { ServersService } from './services/servers.service';
import { ServerFormComponent } from './components/server-form/server-form.component';
import { ServerResolve } from './server.resolve';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
  ],
  declarations: [
    ServersListComponent,
    ServersSidenavComponent,
    ServerFormComponent
  ],
  exports: [
    ServersListComponent,
    ServerFormComponent,
  ],
  entryComponents: [ServersSidenavComponent],
  providers: [ServersService, ServerResolve]
})
export class ServersModule { }
