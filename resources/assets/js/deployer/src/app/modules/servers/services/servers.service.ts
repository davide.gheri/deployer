import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Server } from '../models/server';
import { BaseService } from '../../core/services/base.service';

@Injectable()
export class ServersService extends BaseService<Server> {
  private endPoint: string = 'servers';
  private modelName: string = 'Server';

  get servers(): Observable<Server[]> {
    return this.models$.asObservable();
  }

  get server(): Observable<Server> {
    return this.model$.asObservable();
  }

  testConnection(id): Promise<any> {
    return this.api.post(this.resolveUrl('test', id), {}).toPromise()
  }

}
