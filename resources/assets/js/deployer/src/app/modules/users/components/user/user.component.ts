import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../../services/users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NewUser, User } from '../../models/user';
import { BreadcrumbService } from 'ngx-admin-lte';
import 'rxjs/operator/switchMap';
import { CustomValidators } from 'ng2-validation';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy {
  isEdit: boolean;
  userForm: FormGroup;
  private user: User;

  constructor(
    private fb: FormBuilder,
    private users: UsersService,
    private router: Router,
    private route: ActivatedRoute,
    private bread: BreadcrumbService,
  ) { }

  ngOnInit() {
    this.userForm = this.fb.group({
      email: ['', [Validators.required, CustomValidators.email]],
      name: ['', Validators.required],
      password: ['']
    });

    this.route.data.subscribe(data => {
      if (data.user) {
        this.isEdit = true;
        this.user = data.user;
        this.userForm.patchValue(data.user);
        this.bread.setCurrent({
          display: true,
          header: 'Edit user ' + data.user.name,
          levels: [
            {
              icon: 'dashboard',
              link: ['/'],
            },
            {
              icon: 'group',
              link: ['/users'],
              title: 'Users'
            },
            {
              icon: 'user',
              link: [],
              title: data.user.name
            }
          ]
        });
      } else {
        this.isEdit = false;
        this.bread.setCurrent({
          display: true,
          header: 'New user',
          levels: [
            {
              icon: 'dashboard',
              link: ['/'],
            },
            {
              icon: 'group',
              link: ['/users'],
              title: 'Users'
            },
            {
              icon: 'user',
              link: [],
              title: 'New'
            }
          ]
        });
      }
    });
  }

  ngOnDestroy() {
    this.bread.clear();
  }

  onSubmit() {
    if (this.userForm.valid) {
      if (this.isEdit) {
        this.updateUser();
      } else {
        this.createUser();
      }
    } else {
      Object.keys(this.userForm.controls).forEach(name => {
        this.userForm.get(name).markAsTouched()
      })
    }
  }

  private updateUser() {
    const data: NewUser = this.userForm.value;
    if (data['password'] === '') {
      delete data['password'];
    }
    this.users.update(this.user.id, data)
      .then(res => {
        this.router.navigate(['/users'])
      })
      .catch(err => this.handleErrors(err))
  }

  private createUser() {
    this.users.create(<NewUser>this.userForm.value)
      .then(res => {
        this.router.navigate(['/users/' + res.id]);
      })
      .catch(err => this.handleErrors(err))
  }

  private handleErrors(err) {
    if (err.status === 422) {
      const { errors } = err.error;
      Object.keys(errors).forEach(field => {
        if (this.userForm.get(field)) {
          this.userForm.get(field).setErrors({error: errors[field][0]})
        }
      });
    }
  }

  onDelete() {
    const c = confirm('Are you sure?');
    if (c) {
      this.users.delete(this.user.id)
        .then(res => this.router.navigate(['/users']));
    }
  }

  get emailCtrl(): FormControl {
    return this.userForm.get('email') as FormControl;
  }

  get passCtrl(): FormControl {
    return this.userForm.get('password') as FormControl;
  }

  get nameCtrl(): FormControl {
    return this.userForm.get('name') as FormControl;
  }

}
