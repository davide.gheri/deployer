import { Component, OnDestroy, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { BreadcrumbService } from 'ngx-admin-lte';
import { User } from '../../models/user';
import { Router } from '@angular/router';
import { TranslateService } from 'ngx-admin-lte';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {
  public param = {model: 'utente'};

  constructor(
    public users: UsersService,
    private bread: BreadcrumbService,
    private router: Router,
    private trans: TranslateService
  ) {
  }

  ngOnInit() {
    this.bread.setCurrent({
      display: true,
      header: 'Users',
      levels: [
        {
          icon: 'dashboard',
          link: ['/'],
        },
        {
          icon: 'group',
          link: ['/users'],
          title: 'Users'
        }
      ]
    });
    this.users.getAll();
  }

  ngOnDestroy() {
    this.bread.clear();
  }

  deleteUser(e, user: User) {
    e.preventDefault();
    const c = confirm('Are you sure?');
    if (c) {
      this.users.delete(user.id)
        .then(res => this.router.navigate(['/users']))
    }
  }

}
