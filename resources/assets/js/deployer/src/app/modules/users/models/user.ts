
export interface User {
  id?: number;
  email?: string;
  name?: string;
  avatar?: string;
}

export interface NewUser {
  email: string;
  name: string;
  password?: string;
}