import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { User } from '../models/user';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/do';
import { BaseService } from '../../core/services/base.service';

@Injectable()
export class UsersService extends BaseService<User> {
  private endPoint: string = 'users';
  private modelName: string = 'User';

  get users(): Observable<User[]> {
    return this.models$.asObservable();
  }

  get user(): Observable<User> {
    return this.model$.asObservable();
  }

}
