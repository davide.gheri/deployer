import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { User} from './models/user';
import { Injectable } from '@angular/core';
import { RouterStateSnapshot } from '@angular/router/src/router_state';
import { Observable } from 'rxjs/Observable';
import { UsersService } from './services/users.service';

@Injectable()
export class UserResolve implements Resolve<User> {
  constructor(
    private users: UsersService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> {
    const id = route.paramMap.get('id');
    return this.users.get(id);
  }
}
