import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './components/users/users.component';
import { CoreModule } from '../core/core.module';
import { UsersService } from './services/users.service';
import { UserComponent } from './components/user/user.component';
import { UserResolve } from './user.resolve';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
  ],
  declarations: [UsersComponent, UserComponent],
  providers: [UsersService, UserResolve]
})
export class UsersModule { }
