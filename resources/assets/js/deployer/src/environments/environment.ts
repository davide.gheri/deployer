// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

const api = {
  host: 'deployer.test',
  isHttps: false,
  version: 'v1',
  namespace: 'api',
  authPrefix: 'auth',
  socketEndpoint: ':6001'
};

export function apiUrl(uri = '') {
  return `http${api.isHttps ? 's' : ''}://${api.host}/${api.namespace}/${api.version}/${uri}`;
}

export const environment = {
  production: false,
  host: api.host,
  namespace: api.namespace,
  isHttps: api.isHttps,
  version: api.version,
  socketEndpoint: api.socketEndpoint,
  baseUrl: apiUrl(),
  authPrefix: api.authPrefix,
  api: {
    auth: {
      login: apiUrl(api.authPrefix + '/login'),
      logout: apiUrl(api.authPrefix + '/logout'),
      refresh: apiUrl(api.authPrefix + '/refresh'),
    },
    user: apiUrl('me'),
    info: apiUrl('info'),
    users: {
      index: apiUrl('users'),
      store: apiUrl('users'),
      show: apiUrl('users/:id'),
      update: apiUrl('users/:id'),
      destroy: apiUrl('users/:id'),
    },
    projects: {
      index: apiUrl('projects'),
      store: apiUrl('projects'),
      show: apiUrl('projects/:id'),
      update: apiUrl('projects/:id'),
      destroy: apiUrl('projects/:id'),
      updateOptions: apiUrl('projects/:id/updateOptions')
    },
    deployments: {
      index: apiUrl('projects/:parent_id/deployments'),
      show: apiUrl('projects/:parent_id/deployments/:id'),
      store: apiUrl('projects/:parent_id/deployments'),
    },
    servers: {
      index: apiUrl('servers'),
      store: apiUrl('servers'),
      show: apiUrl('servers/:id'),
      update: apiUrl('servers/:id'),
      destroy: apiUrl('servers/:id'),
      test: apiUrl('servers/:id/test')
    },
    deployTargets: {
      index: apiUrl('projects/:parent_id/deployTargets'),
      store: apiUrl('projects/:parent_id/deployTargets'),
      show: apiUrl('projects/:parent_id/deployTargets/:id'),
      update: apiUrl('projects/:parent_id/deployTargets/:id'),
      destroy: apiUrl('projects/:parent_id/deployTargets/:id')
    },
    deploySteps: {
      index: apiUrl('projects/:parent_id/deploySteps'),
      store: apiUrl('projects/:parent_id/deploySteps'),
      show: apiUrl('projects/:parent_id/deploySteps/:id'),
      update: apiUrl('projects/:parent_id/deploySteps/:id'),
      destroy: apiUrl('projects/:parent_id/deploySteps/:id')
    },
    commands: {
      index: apiUrl('projects/:parent_id/deploySteps/:sub_parent_id/commands'),
      store: apiUrl('projects/:parent_id/deploySteps/:sub_parent_id/commands'),
      update: apiUrl('projects/:parent_id/deploySteps/:sub_parent_id/commands/:id'),
      destroy: apiUrl('projects/:parent_id/deploySteps/:sub_parent_id/commands/:id')
    }
  }
};
