# [GIT] Mirroring the repo...
echo "[GIT] Mirroring the repo..."
chmod +x {{ wrapper_file }}
export GIT_SSH={{ wrapper_file }}

if [ ! -d {{ mirror_path }} ]; then
    git clone --mirror {{ repository }} {{ mirror_path }}
else
    echo "Mirror already exists, skipping clone"
fi

cd {{ mirror_path }}
echo "[GIT] Fetching the repo..."
git fetch --all --prune