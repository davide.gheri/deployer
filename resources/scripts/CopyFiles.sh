#!/usr/bin/env bash
echo "[SSH:Copy] Copying files..."

declare -A files=( {{ files }} )

cd {{ release_path }}

for origin in "${!files[@]}"; do
    if [ -f $origin ]; then
        cp $origin ${files[$origin]}
    fi
done

echo "[SSH:Copy] Done"
