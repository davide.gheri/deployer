#[GIT] Creating a tar.gz archive for the release...
echo "[GIT] Creating a tar.gz archive for the release..."
echo "https://github.com/meitar/git-archive-all.sh"
git clone --depth 1 --recursive {{ mirror_path }} {{ tmp_path }}
cd {{ tmp_path }}
git checkout {{ sha }}
mkdir --parents {{ release_archive_parent }}
#
# GitArchiveAll.sh https://github.com/meitar/git-archive-all.sh
#
{{ scripts_path }}/GitArchiveAll.sh --tree-ish {{ sha }} --format tar.gz --verbose {{ release_archive }}
cd -
rm -rf {{ tmp_path }}
echo "[GIT] Done"