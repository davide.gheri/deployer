### [SSH] Extracting...
echo "[SSH:Extract] Extracting..."

cd {{ project_path }}

if [ ! -d {{ releases_path }} ]; then
    echo "Releases path does not exists, creating..."
    mkdir {{ releases_path }}
fi

#if [ ! -d {{ shared_path }} ]; then
#    mkdir {{ shared_path }}
#fi

mkdir {{ release_path }}
cd {{ release_path }}
echo "Extracting the release archive..."
tar -m --gunzip --extract --file {{ remote_archive }} --directory {{ release_path }}

rm -f {{ remote_archive }}
echo "[SSH:Extract] Done"