### Generate SSH Key
echo "Deploy key"

if [ -f {{ key_file }} ]; then
    rm -f {{ key_file }}
fi

ssh-keygen -q -t rsa -b 2048 -f {{ key_file }} -N "" -C "Deployer - ALL"
