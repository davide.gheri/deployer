#!/usr/bin/env bash

echo "[SSH:CLEAN] Removing old releases... Keeping last {{ to_keep }}"

cd {{ releases_path }}
(ls -t|head -n {{ to_keep }};ls)|sort|uniq -u|xargs rm -rf

echo "[SSH:CLEAN] Done."
