### [SSH] Sending file to server...
echo "[SSH:Send] Sending file to server {{ deploy_target }} ..."
rsync --verbose --compress --progress --out-format="Receiving %n" -e \
    "ssh -p {{ port }} \
         -o CheckHostIP=no \
         -o IdentitiesOnly=yes \
         -o StrictHostKeyChecking=no \
         -o PasswordAuthentication=no \
         -o IdentityFile={{ private_key }}" \
    {{ local_file }} {{ user }}@{{ ip_address }}:{{ remote_file }}
echo "[SSH:Send] Done"