#!/usr/bin/env bash
echo "[SSH:Share] Linking shared folder {{ folder }} ..."
if [ ! -d {{ shared_dir }}/{{ folder }} ]; then
    echo "Project shared dir does not exists, creating..."
    mkdir -p {{ shared_dir }}/{{ folder }}

    if [ -d {{ release_dir }}/{{ folder }} ]; then
        cp -pRn {{ release_dir }}/{{ folder }} {{ shared_dir }}
    fi
fi

if [ -d {{ release_dir }}/{{ folder }} ]; then
    rm -rf {{ release_dir }}/{{ folder }}
fi

ln -s {{ shared_dir }}/{{ folder }} {{ release_dir }}/{{ folder }}

echo "[SSH:Share] Done folder {{ folder }}"
