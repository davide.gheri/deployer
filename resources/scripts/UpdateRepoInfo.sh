# [GIT] Get last commit info...
#echo "[GIT] Get last commit info..."
cd {{ mirror_path }}

git log {{ git_ref }} -n1 --pretty=format:"%H%x09%an%x09%ae%x09%s"