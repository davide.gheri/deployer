<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Deployer</title>
    <base href="/">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    {{--<script type="text/javascript" src="/js/socket.io.js"></script>--}}
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    <script>
        // if (typeof io !== 'undefined') {
        //     window.io = io;
        // } else {
        //     console.error('socket.io not found!');
        // }
        window.__SERVER__= {
            host: '{{app('request')->server('HTTP_HOST')}}',
            sslEnabled: {{config('app.env') == 'production' ? 'true' : 'false'}}
        }
    </script>
    <link href="/css/styles.bundle.css" rel="stylesheet"/>
</head>
<body>
<app-root></app-root>
<script type="text/javascript" src="/js/inline.bundle.js"></script>
<script type="text/javascript" src="/js/polyfills.bundle.js"></script>
<script type="text/javascript" src="/js/scripts.bundle.js"></script>
<script type="text/javascript" src="/js/main.bundle.js"></script></body>
</html>
