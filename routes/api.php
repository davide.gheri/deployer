<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['namespace' => 'Api\V1', 'as' => 'api.', 'prefix' => 'v1'], function() {
    Route::prefix('auth')->group(function() {
        Route::post('login', 'AuthController@login')->name('login');
        Route::post('refresh', 'AuthController@refresh')->name('refresh');
        Route::middleware('auth:api')->post('logout', 'AuthController@logout')->name('logout');
    });

    Route::middleware('auth:api')->get('me', function(Request $request) {
        return $request->user();
    })->name('me');

    Route::get('info', function() {
        return response()->json([
            'siteName' => config('app.name'),
            'sshKey' => config('deployer.ssh.fingerprint')
        ]);
    });

    Route::group(['middleware' => 'auth:api'], function() {
        Route::apiResource('users', 'UserController');
        Route::patch('projects/{id}/updateOptions', 'ProjectController@updateOptions');
        Route::apiResource('projects', 'ProjectController');
        Route::apiResource('projects.deployments', 'DeploymentController');
        Route::apiResource('projects.deployTargets', 'DeployTargetController');
        Route::apiResource('projects.deploySteps', 'DeployStepController');
        Route::apiResource('projects.deploySteps.commands', 'CommandController');
        Route::post('servers/{server}/test', 'ServerController@test')->name('servers.test');
        Route::apiResource('servers', 'ServerController');
    });
});
