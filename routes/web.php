<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{args}', function () {
    return view('home');
})->where(['args' => '(.*)']);

Route::group(['namespace' => 'Webhooks'], function() {
    Route::post('{id}', 'ProjectWebhookController');
});